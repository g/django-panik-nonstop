import unicodedata
import urllib.parse

import requests
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from emissions.utils import whatsonair

from nonstop.utils import get_current_nonstop_track


class Command(BaseCommand):
    def handle(self, *args, **options):
        onair = whatsonair()
        onair_txt = None
        if onair.get('episode'):
            onair_txt = '%s - %s' % (onair.get('emission').title, onair.get('episode').title)
        elif onair.get('emission'):
            onair_txt = onair.get('emission').title
        elif onair.get('nonstop'):
            track = get_current_nonstop_track()
            if 'track_artist' in track:
                onair_txt = '%s - %s (%s)' % (
                    onair.get('nonstop').title,
                    track.get('track_title'),
                    track.get('track_artist'),
                )
            elif 'track_title' in track:
                onair_txt = '%s - %s' % (onair.get('nonstop').title, track.get('track_title'))
            else:
                onair_txt = onair.get('nonstop').title
        if not onair_txt:
            return

        for stream_url in settings.STREAM_UPDATE_URLS:
            if '.aac' in stream_url:
                onair_msg = unicodedata.normalize('NFKD', onair_txt).encode('ascii', 'ignore')
            else:
                onair_msg = onair_txt.encode('utf-8')
            try:
                r = requests.get(stream_url + '&song=' + urllib.parse.quote(onair_msg), timeout=5)
                r.raise_for_status()
            except Exception as e:
                print('failed to update', e)

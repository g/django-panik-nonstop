import asyncio
import json
import logging

import aiohttp.web
import requests
import websockets
from django.core.management.base import BaseCommand

from nonstop.app_settings import app_settings

logger = logging.getLogger('switch-proxy')


class PanikSwitchProtocol(asyncio.DatagramProtocol):
    def __init__(self):
        super().__init__()
        self.currently_active = 0
        self.update_with_http()
        self.websocket_connections = set()

    def datagram_received(self, data, addr):
        logger.debug('Datagram received: %s', data)
        new_active = json.loads(data)['active']
        if new_active != self.currently_active:
            logger.info('UDP update, %s -> %s', self.currently_active, new_active)
        self.currently_active = json.loads(data)['active']

    async def websocket_handler(self, websocket, path):
        self.websocket_connections.add(websocket)
        latest_active = None
        while True:
            if self.currently_active != latest_active:
                latest_active = self.currently_active
                to_trash = []
                for ws in self.websocket_connections:
                    try:
                        await ws.send(json.dumps({'active': self.currently_active}))
                    except websockets.exceptions.ConnectionClosed:
                        to_trash.append(ws)
                for ws in to_trash:
                    self.websocket_connections.remove(ws)
            await asyncio.sleep(0.1)

    def update_with_http(self):
        try:
            resp = requests.get(app_settings.ON_AIR_SWITCH_URL, timeout=2)
            if resp.ok:
                new_active = resp.json().get('active')
                if new_active != self.currently_active:
                    logger.info('HTTP-only update, %s -> %s', self.currently_active, new_active)
                self.currently_active = resp.json().get('active')
        except (OSError, requests.exceptions.RequestException):
            pass

    def handle_http(self, request):
        resp = {
            'response': 0,
            'active': self.currently_active,
            'nonstop-via-stud1': 0,
            'nonstop-via-stud2': 0,
        }
        return aiohttp.web.Response(text=json.dumps(resp), content_type='application/json')


class Command(BaseCommand):
    help = 'proxy between arduino switch and websockets'

    def add_arguments(self, parser):
        parser.add_argument('--udp-port', metavar='UDP_PORT', default='1312')
        parser.add_argument('--ws-port', metavar='WS_PORT', default='8765')
        parser.add_argument('--http-port', metavar='HTTP_PORT', default='8766')

    def handle(self, verbosity, **options):
        self.udp_port = int(options.get('udp_port'))
        self.ws_port = int(options.get('ws_port'))
        self.http_port = int(options.get('http_port'))
        asyncio.run(self.main())

    async def main(self):
        loop = asyncio.get_running_loop()
        proto = PanikSwitchProtocol()

        await websockets.serve(proto.websocket_handler, '0.0.0.0', self.ws_port)

        app = aiohttp.web.Application()
        app.add_routes([aiohttp.web.get('/', proto.handle_http)])
        runner = aiohttp.web.AppRunner(app)
        await runner.setup()
        site = aiohttp.web.TCPSite(runner, port=self.http_port)
        await site.start()
        logger.info(f'HTTP server listening on 0.0.0.0:{self.http_port}')

        transport, protocol = await loop.create_datagram_endpoint(
            lambda: proto, local_addr=('0.0.0.0', self.udp_port)
        )
        try:
            while True:
                await asyncio.sleep(300)
                proto.update_with_http()
        finally:
            transport.close()

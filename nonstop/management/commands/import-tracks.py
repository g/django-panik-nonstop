import datetime
import os

import mutagen
from django.core.files.storage import default_storage
from django.core.management.base import BaseCommand
from django.utils.timezone import now
from emissions.models import Nonstop
from emissions.utils import get_duration

from ...models import Artist, Track


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--directory', metavar='DIRECTORY')
        parser.add_argument('--zone', metavar='ZONE')
        parser.add_argument(
            '--language', metavar='LANGUAGE', help='Mark with given language (en, fr, nl, other, na)'
        )
        parser.add_argument('--instru', action='store_true', default=False, help='Mark as instrumental')
        parser.add_argument('--cfwb', action='store_true', default=False, help='Mark as CFWB')
        parser.add_argument('--no-sabam', action='store_true', default=False, help='Mark as non-sabam')

    def handle(
        self, verbosity, directory, zone=None, language=None, instru=None, cfwb=None, no_sabam=None, **kwargs
    ):
        monthdir = datetime.datetime.today().strftime('%Y-%m')

        nonstop_zone = Nonstop.objects.get(slug=zone) if zone else None

        filepaths = []
        for basedir, dirnames, filenames in os.walk(directory):
            filepaths.extend(
                [
                    os.path.join(basedir, x)
                    for x in filenames
                    if os.path.splitext(x)[-1] in ('.mp3', '.ogg', '.flac', '.opus')
                ]
            )
        filepaths.sort()

        total = len(filepaths)

        for i, filepath in enumerate(filepaths):
            try:
                metadata = mutagen.File(filepath, easy=True)
            except:
                print('failed to get metadata', filepath)
                continue
            if not metadata or not metadata.get('artist') or not metadata.get('title'):
                print('missing metadata', filepath)
                continue

            artist_name = metadata.get('artist')[0]
            track_title = metadata.get('title')[0]

            if verbosity:
                print('[%s/%s] %s - %s' % (i + 1, total, artist_name, track_title))

            new_filepath = '%s/%s - %s - %s%s' % (
                monthdir,
                datetime.datetime.today().strftime('%y%m%d'),
                artist_name[:50].replace('/', ' ').strip(),
                track_title[:80].replace('/', ' ').strip(),
                os.path.splitext(filepath)[-1],
            )

            artist, created = Artist.objects.get_or_create(name=artist_name)
            track, created = Track.objects.get_or_create(
                title=track_title,
                artist=artist,
            )
            if created:
                with open(filepath, 'rb') as fd:
                    track.soundfile = default_storage.save(
                        os.path.join('nonstop', 'tracks', new_filepath), content=fd
                    )
                    track.save()

            if not track.duration:
                duration = get_duration(track.file_path())
                if duration:
                    track.duration = datetime.timedelta(seconds=float(duration))

            if nonstop_zone:
                track.nonstop_zones.add(nonstop_zone)
            if not track.added_to_nonstop_timestamp:
                track.added_to_nonstop_timestamp = now()
            track.language = language or track.language
            track.instru = instru or track.instru
            track.cfwb = cfwb or track.cfwb
            track.sabam = False if no_sabam else True
            track.save()

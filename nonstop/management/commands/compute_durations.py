import datetime
import os

from django.core.management.base import BaseCommand
from emissions.utils import get_duration

from ...models import Jingle, Track


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--recent', action='store_true', default=False, help='Only do recent files')
        parser.add_argument('--jingles', action='store_true', default=False, help='Do jingles')
        parser.add_argument(
            '--force', action='store_true', default=False, help='Recompute existing durations'
        )

    def handle(self, verbosity, **kwargs):
        if kwargs.get('jingles'):
            qs = Jingle.objects.all()
        else:
            qs = Track.objects.all()
            if kwargs.get('recent'):
                qs = qs.filter(creation_timestamp__gt=datetime.date.today() - datetime.timedelta(days=10))
        if not kwargs.get('force'):
            qs = qs.filter(duration__isnull=True)
        if verbosity:
            total = qs.count()
        for i, track in enumerate(qs):
            if verbosity:
                print('[%s/%s] %s' % (i, total, track))
            try:
                file_path = track.file_path()
            except AttributeError:
                continue
            if file_path is None or not os.path.exists(file_path):
                continue
            duration = get_duration(file_path)
            if duration:
                track.duration = datetime.timedelta(seconds=float(duration))
                track.save()

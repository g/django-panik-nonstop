import datetime

from django.core.management.base import BaseCommand
from emissions.models import Absence

from nonstop.models import (
    RecurringOccurence,
    RecurringPlaylistDiffusion,
    RecurringRandomDirectoryDiffusion,
    RecurringStreamDiffusion,
)


class Command(BaseCommand):
    def handle(self, verbosity, **kwargs):
        for diffusion in RecurringStreamDiffusion.objects.filter(is_active=True):
            next_planned_date = diffusion.schedule.get_next_planned_date(since=datetime.datetime.now())
            try:
                occurence = RecurringOccurence.objects.get(
                    stream_diffusion=diffusion, datetime=next_planned_date
                )
            except RecurringOccurence.DoesNotExist:
                occurence = RecurringOccurence(stream_diffusion=diffusion, datetime=next_planned_date)
            if (
                occurence.id
                and Absence.objects.filter(
                    emission=diffusion.schedule.emission, datetime=next_planned_date
                ).exists()
            ):
                occurence.delete()
            elif not occurence.id:
                occurence.save()

        for diffusion in RecurringRandomDirectoryDiffusion.objects.filter(is_active=True):
            next_planned_date = diffusion.schedule.get_next_planned_date(since=datetime.datetime.now())
            try:
                occurence = RecurringOccurence.objects.get(
                    directory_diffusion=diffusion, datetime=next_planned_date
                )
            except RecurringOccurence.DoesNotExist:
                occurence = RecurringOccurence(directory_diffusion=diffusion, datetime=next_planned_date)
            if not occurence.id:
                occurence.save()

        for diffusion in RecurringPlaylistDiffusion.objects.filter(is_active=True):
            next_planned_date = diffusion.schedule.get_next_planned_date(since=datetime.datetime.now())
            try:
                occurence = RecurringOccurence.objects.get(
                    playlist_diffusion=diffusion, datetime=next_planned_date
                )
            except RecurringOccurence.DoesNotExist:
                occurence = RecurringOccurence(playlist_diffusion=diffusion, datetime=next_planned_date)
            if not occurence.id:
                occurence.save()

import time

from django.core.management.base import BaseCommand
from django.db.models import Q

from ...models import ScheduledDiffusion
from ...utils import add_soma_diffusion, remove_soma_diffusion


class Command(BaseCommand):
    def handle(self, verbosity, **kwargs):
        diffusions = ScheduledDiffusion.objects.filter(
            Q(diffusion__isnull=True) | Q(added_to_nonstop_timestamp__isnull=True)
        )
        if not diffusions.exists():
            return

        for diffusion in diffusions:
            if diffusion.diffusion_id is None:
                remove_soma_diffusion(diffusion)
                diffusion.delete()
            else:
                add_soma_diffusion(diffusion)
            time.sleep(5)  # give some time to soma (...)

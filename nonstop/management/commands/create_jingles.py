import datetime
import os

from django.core.management.base import BaseCommand, CommandError
from emissions.utils import get_duration

from ...app_settings import app_settings
from ...models import Jingle


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--subdirectory', default=None)

    def handle(self, verbosity, **options):
        self.verbose = int(verbosity) > 1

        base_path = os.path.abspath(os.path.join(app_settings.LOCAL_BASE_PATH, app_settings.JINGLES_PREFIX))
        walk_path = base_path
        if options.get('subdirectory'):
            walk_path = os.path.join(walk_path, options.get('subdirectory'))

        for basedir, dirnames, filenames in os.walk(walk_path):
            for filename in filenames:
                fullpath = os.path.join(basedir, filename)
                filepath = fullpath[len(base_path) + 1 :]
                if Jingle.objects.filter(filepath=filepath).exists():
                    continue
                jingle = Jingle(label=filename, filepath=filepath)
                jingle.duration = datetime.timedelta(seconds=float(get_duration(fullpath)))
                jingle.save()

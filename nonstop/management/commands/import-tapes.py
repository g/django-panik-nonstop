import datetime
import os

from django.core.files.storage import default_storage
from django.core.management.base import BaseCommand
from django.utils.timezone import now
from emissions.models import Nonstop
from emissions.utils import get_duration

from nonstop.app_settings import app_settings
from nonstop.models import Artist, Tape, TapePart, Track


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--directory', metavar='DIRECTORY')
        parser.add_argument('--zone', metavar='ZONE')

    def handle(self, verbosity, directory, zone=None, **kwargs):
        monthdir = datetime.datetime.today().strftime('%Y-%m')

        nonstop_zone = Nonstop.objects.get(slug=zone) if zone else None
        filepaths = []
        for basedir, dirnames, filenames in os.walk(directory):
            filepaths.extend(
                [os.path.join(basedir, x) for x in filenames if os.path.splitext(x)[-1] == '.csv']
            )
        filepaths.sort()

        for i, csv_filepath in enumerate(filepaths):
            audio_filepath = None
            for audio_suffix in ('.wav', '.mp3', '.ogg', '.opus', '.flac'):
                audio_filepath = f'{csv_filepath.removesuffix(".csv")}{audio_suffix}'
                if os.path.exists(audio_filepath):
                    break
                audio_filepath = None

            if not audio_filepath:
                continue

            new_filepath = '%s/%s - %s' % (
                monthdir,
                datetime.datetime.today().strftime('%y%m%d'),
                os.path.basename(audio_filepath),
            )

            with open(csv_filepath) as fd:
                lines = [x.strip().split(';') for x in fd.readlines()]

            if lines[0][0][0] != '#':
                print(f'error in file {csv_filepath}, missing #header')
                continue

            tape_title = lines[0][0].removeprefix('#').strip()
            tape_title += ' (%s)' % os.path.basename(csv_filepath).removesuffix('.csv')

            artist, created = Artist.objects.get_or_create(
                name=app_settings.TRACK_UNKNOWN_ARTIST_LABEL or 'Unknown'
            )
            tape_track, created = Track.objects.get_or_create(
                title=tape_title,
                artist=artist,
            )
            if created:
                with open(audio_filepath, 'rb') as fd:
                    tape_track.soundfile = default_storage.save(
                        os.path.join('nonstop', 'tracks', new_filepath), content=fd
                    )
                    tape_track.save()
            if not tape_track.duration:
                duration = get_duration(tape_track.file_path())
                if duration:
                    tape_track.duration = datetime.timedelta(seconds=float(duration))
                    tape_track.save()

            tape, created = Tape.objects.get_or_create(full_track_id=tape_track.id)
            if created:
                tape.save()
            TapePart.objects.filter(tape=tape).delete()
            for track_line in lines[1:]:
                artist, title, minutes, seconds = track_line[:4]
                if artist.lower() == 'jingle':
                    continue
                artist, created = Artist.objects.get_or_create(name=artist)
                track, created = Track.objects.get_or_create(
                    title=title,
                    artist=artist,
                )
                if len(track_line) == 5:
                    tags = track_line[-1].lower()
                    if 'fwb' in tags:
                        track.cfwb = True
                    if 'fr' in tags:
                        track.language = 'fr'
                    track.save()
                position = datetime.timedelta(minutes=int(minutes), seconds=int(seconds))
                TapePart.objects.create(tape=tape, track=track, position=position)

            if nonstop_zone:
                tape_track.nonstop_zones.add(nonstop_zone)
            if not track.added_to_nonstop_timestamp:
                tape_track.added_to_nonstop_timestamp = now()
            tape_track.save()

            print('done processing', csv_filepath)

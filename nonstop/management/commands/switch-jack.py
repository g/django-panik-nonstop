import asyncio
import json
import logging

import aiohttp
from django.core.management.base import BaseCommand, CommandError

from nonstop.app_settings import app_settings

try:
    import jack
except ImportError:
    jack = None

logger = logging.getLogger('switch-jack')


class ClientWebSocketResponse(aiohttp.client_ws.ClientWebSocketResponse):
    async def pong(self, message):
        # update jack connections regularly, in case of jack ports going down momentarily
        self.command.update_jack_connections(self.command.currently_active)
        await super().pong(message)


class Command(BaseCommand):
    help = 'jack source switch'

    def handle(self, verbosity, **options):
        if jack is None:
            raise CommandError('missing jack module (install python3-jack-client?)')
        self.verbosity = verbosity
        asyncio.run(self.main())

    async def main(self):
        if not app_settings.SWITCH_WS_URL:
            raise CommandError('missing switch_ws_url')
        self.currently_active = None
        sleep_duration = 0.2
        ClientWebSocketResponse.command = self
        while True:
            try:
                async with aiohttp.ClientSession(ws_response_class=ClientWebSocketResponse) as session:
                    async with session.ws_connect(app_settings.SWITCH_WS_URL, heartbeat=5) as ws:
                        logger.info('waiting for messages')
                        sleep_duration = 0.2  # reset sleep duration to baseline
                        async for msg in ws:
                            if msg.type == aiohttp.WSMsgType.TEXT:
                                try:
                                    msg = json.loads(msg.data)
                                except ValueError:
                                    continue
                                if msg.get('active') != self.currently_active:
                                    self.currently_active = msg.get('active')
                                    logger.info('setting source: %s', self.currently_active)
                                    self.update_jack_connections(self.currently_active)
                            elif msg.type == aiohttp.WSMsgType.ERROR:
                                break
                    logger.debug('lost websocket connection')
            except aiohttp.ClientError as e:
                logger.warning('websocket error (%s)' % e)
                sleep_duration *= 2
                await asyncio.sleep(min(sleep_duration, 5))
            except asyncio.CancelledError:
                # most probably because of a keyboard interrupt, quit silently
                return

    def update_jack_connections(self, active):
        if active not in app_settings.SWITCH_IN_PORTS:
            logger.info('unsupported source: %s', active)
            return
        out_ports = app_settings.SWITCH_OUT_PORTS
        with jack.Client('switch-jack') as client:
            known_ports = {x.name for x in client.get_ports(is_audio=True)}
            for dports in out_ports.values():
                if any(x not in known_ports for x in dports):
                    logger.error('unavailable destination ports %r', dports)
                    continue
                for port_id, port_names in app_settings.SWITCH_IN_PORTS.items():
                    if any(x not in known_ports for x in port_names):
                        logger.error('unavailable source ports %r', port_names)
                        continue
                    try:
                        if port_id == active:
                            self.jack_connect(client, port_names[0], dports[0])
                            self.jack_connect(client, port_names[1], dports[1])
                        else:
                            self.jack_disconnect(client, port_names[0], dports[0])
                            self.jack_disconnect(client, port_names[1], dports[1])
                    except jack.JackError as e:
                        logger.error('jack error: %s' % e)

    def jack_connect(self, client, in_port, out_port):
        connections = [x.name for x in client.get_all_connections(in_port)]
        if out_port not in connections:
            if self.verbosity:
                logger.info('connecting %s and %s', in_port, out_port)
            client.connect(in_port, out_port)

    def jack_disconnect(self, client, in_port, out_port):
        connections = [x.name for x in client.get_all_connections(in_port)]
        if out_port in connections:
            if self.verbosity:
                logger.info('disconnecting %s and %s', in_port, out_port)
            client.disconnect(in_port, out_port)

import datetime
import html
import json
import logging
import time

import requests
from django.core.management.base import BaseCommand, CommandError
from django.utils.dateparse import parse_datetime

from nonstop.models import Artist, SomaLogLine, Track

logger = logging.getLogger('airtime_tracker')


def parse_duration(duration):
    # ex: "00:01:30.07025" -> 90.07025 (seconds)
    parts = [float(x) for x in duration.split(':')]
    return datetime.timedelta(seconds=parts[2] + (parts[1] * 60) + (parts[0] * 60 * 60))


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--live-info-url', help='https://foobar.airtime.pro/api/live-info-v2')
        parser.add_argument('--channel', metavar='CHANNEL')
        parser.add_argument('--ignore-duration-checks', action='store_true', default=False)

    def handle(self, verbosity, live_info_url, channel=None, ignore_duration_checks=False, **options):
        if not live_info_url:
            raise CommandError('missing --live-info-url')

        # run until killed, and starts back where it was killed
        last_skipped = None
        while True:
            logger.debug('Checking API')
            try:
                resp = requests.get(live_info_url, timeout=10)
            except requests.RequestException as err:
                logger.error('Got error requesting API (%s)', err)
                time.sleep(60)
                continue
            if not resp.ok:
                logger.error('Got invalid API answer (%s)', resp.status_code)
                time.sleep(60)
                continue
            try:
                current_show = (resp.json().get('shows') or {}).get('current')
            except json.JSONDecodeError:
                logger.error('Got invalid API answer (not JSON)')
                time.sleep(60)
                continue
            if current_show and current_show.get('auto_dj'):
                # API returns metadata for previous/next track and a useless
                # "livestream" entry for the currently playing track; so we
                # look at them all.
                logger.debug('(autodj mode)')
                parts = ('previous', 'current', 'next')
            elif current_show:
                parts = ('current',)
            else:
                parts = ()
            for part_name in parts:
                part = resp.json().get('tracks', {}).get(part_name)
                if part is None:
                    continue
                starts = part.get('starts')
                if part.get('type') == 'track':
                    metadata = part.get('metadata')
                    if not metadata.get('length'):
                        continue
                    if not metadata.get('track_title'):
                        continue
                    duration = parse_duration(metadata.get('length'))
                    if duration < datetime.timedelta(seconds=30) and not ignore_duration_checks:
                        log = logger.debug
                        if last_skipped != metadata.get('track_title'):
                            last_skipped = metadata.get('track_title')
                            log = logger.info
                        log(
                            'Got %s but skipped as too short (%s seconds)',
                            metadata.get('track_title'),
                            duration.total_seconds(),
                        )
                        continue  # skip what's probably a jingle
                    if duration > datetime.timedelta(seconds=3000) and not ignore_duration_checks:
                        log = logger.debug
                        if last_skipped != metadata.get('track_title'):
                            last_skipped = metadata.get('track_title')
                            log = logger.info
                        log(
                            'Got %s but skipped as too long (%s seconds)',
                            metadata.get('track_title'),
                            duration.total_seconds(),
                        )
                        continue  # skip what's probably a full show
                    if metadata.get('artist_name'):
                        artist, created = Artist.objects.get_or_create(
                            name=html.unescape(metadata.get('artist_name'))
                        )
                    else:
                        continue  # skip if there's no artist in metadata
                    last_skipped = None
                    track, created = Track.objects.get_or_create(
                        artist=artist,
                        title=html.unescape(metadata.get('track_title')),
                        duration=duration,
                    )
                    if created:
                        logger.info('New track: %s', track)
                    logline, created = SomaLogLine.objects.get_or_create(
                        track=track, play_timestamp=parse_datetime(starts), channel=channel
                    )
                    if created:
                        logger.info('Playing at %s: %s', starts, track)

            if not resp.json().get('tracks', {}).get('next'):
                logger.warning('No known next track')
                time.sleep(30)
                continue

            next_datetime = parse_datetime(resp.json().get('tracks', {}).get('next', {}).get('starts'))
            wait_until = min(datetime.datetime.now() + datetime.timedelta(minutes=5), next_datetime)
            waiting_time = (wait_until - datetime.datetime.now()).total_seconds()
            # wait at least 1 second but maximum 5 minutes
            waiting_time = max((min((waiting_time, 300)), 1))
            logger.debug('Waiting for %d seconds (announced change is %s)', waiting_time, next_datetime)
            time.sleep(waiting_time)

from django.contrib import admin

from .models import (
    Jingle,
    NonstopZoneSettings,
    RecurringRandomDirectoryDiffusion,
    RecurringStreamDiffusion,
    Stream,
)


@admin.register(Jingle)
class JingleAdmin(admin.ModelAdmin):
    pass


@admin.register(Stream)
class StreamAdmin(admin.ModelAdmin):
    pass


@admin.register(NonstopZoneSettings)
class NonstopZoneSettingsAdmin(admin.ModelAdmin):
    pass


@admin.register(RecurringStreamDiffusion)
class RecurringStreamDiffusionAdmin(admin.ModelAdmin):
    pass


@admin.register(RecurringRandomDirectoryDiffusion)
class RecurringRandomDirectoryDiffusionAdmin(admin.ModelAdmin):
    pass

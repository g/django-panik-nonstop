import collections
import copy
import csv
import datetime
import logging
import os
import subprocess
import tempfile
import urllib.parse
from io import StringIO

import mutagen
from django.conf import settings
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.core.files.storage import default_storage
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import Avg, Q, Sum
from django.http import FileResponse, Http404, HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse, reverse_lazy
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from django.views.generic.base import RedirectView, TemplateView
from django.views.generic.dates import DayArchiveView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, FormView, UpdateView
from django.views.generic.list import ListView
from emissions.models import Diffusion, Emission, Nonstop
from emissions.utils import period_program

from . import utils
from .app_settings import app_settings
from .forms import (
    ArtistForm,
    ArtistSearchForm,
    CleanupForm,
    RecurringDeleteForm,
    RecurringPlaylistAddForm,
    RecurringPlaylistEditForm,
    RecurringStreamForm,
    TapeTrackEditForm,
    TapeTrackForm,
    TrackArtistTitleForm,
    TrackMetaForm,
    TrackNonstopForm,
    TracksAddToZoneForm,
    TrackSearchForm,
    UploadTracksForm,
    ZoneSettingsForm,
)
from .models import (
    Artist,
    Jingle,
    NonstopZoneSettings,
    RecurringPlaylistDiffusion,
    RecurringStreamDiffusion,
    ScheduledDiffusion,
    SomaLogLine,
    Stream,
    Tape,
    TapePart,
    Track,
)

logger = logging.getLogger('panikdb')


class SomaDayArchiveView(DayArchiveView):
    queryset = SomaLogLine.objects.all().select_related('track', 'track__artist')
    date_field = "play_timestamp"
    make_object_list = True
    allow_future = False
    month_format = '%m'


class SomaDayArchiveCsvView(SomaDayArchiveView):
    queryset = SomaLogLine.objects.all().select_related('track', 'track__artist')
    ordering = 'play_timestamp'

    def render_to_response(self, context, **response_kwargs):
        out = StringIO()
        writer = csv.writer(out)
        columns = [
            _('date'),
            _('time'),
            _('title'),
            _('artist'),
            _('duration'),
            _('music'),
            _('lyrics'),
            _('French'),
        ]
        if app_settings.INCLUDE_CFWB_META:
            columns.append('FWB')
        if app_settings.INCLUDE_GENDER_META:
            columns.append(_('(cis)Male members'))
            columns.append(_('(cis)Female members'))
            columns.append(_('Non-binary, trans or agender members'))
            columns.append(_('Lead'))
            columns.append('%M')
            columns.append('%F')
            columns.append('%NB')
        writer.writerow(columns)
        for line in context['object_list']:
            if line.track:
                row = [
                    line.play_timestamp.strftime('%d/%m/%Y'),
                    line.play_timestamp.strftime('%H:%M'),
                    line.track.title,
                    line.track.artist.name if line.track.artist else '-',
                    '%d:%d'
                    % (
                        int(line.track.duration.total_seconds() // 60),
                        int(line.track.duration.total_seconds() % 60),
                    )
                    if line.track.duration
                    else '',
                    '1',  # we only export music
                    '0' if line.track.instru else '1',
                    '1' if line.track.language == 'fr' else '0',
                ]
                if app_settings.INCLUDE_CFWB_META:
                    row.append('1' if line.track.cfwb else '0')
                if app_settings.INCLUDE_GENDER_META:
                    row.append('' if line.track.gender_m is None else str(line.track.gender_m))
                    row.append('' if line.track.gender_f is None else str(line.track.gender_f))
                    row.append('' if line.track.gender_x is None else str(line.track.gender_x))
                    row.append('' if line.track.gender_l is None else str(line.track.gender_l))
                    row.append(
                        ''
                        if line.track.gender_m_computed_score is None
                        else '%.1f' % (100 * line.track.gender_m_computed_score)
                    )
                    row.append(
                        ''
                        if line.track.gender_f_computed_score is None
                        else '%.1f' % (100 * line.track.gender_f_computed_score)
                    )
                    row.append(
                        ''
                        if line.track.gender_x_computed_score is None
                        else '%.1f' % (100 * line.track.gender_x_computed_score)
                    )
                writer.writerow(row)

        return HttpResponse(out.getvalue(), content_type='text/csv; charset=utf-8')


class RedirectTodayView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        today = datetime.datetime.today()
        return reverse('archive_day', kwargs={'year': today.year, 'month': today.month, 'day': today.day})


class TrackDetailView(DetailView):
    model = Track

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        if self.object.tape_set.exists():
            ctx['metadata_form'] = TrackNonstopForm(instance=self.object)
        else:
            ctx['metadata_form'] = TrackMetaForm(instance=self.object)
        ctx['tape_mode_enabled'] = app_settings.ENABLE_TAPE_MODE
        return ctx

    def post(self, request, *args, **kwargs):
        assert self.request.user.has_perm('nonstop.add_track')
        instance = self.get_object()
        previous_zones = {x.title for x in instance.nonstop_zones.all()}
        if instance.tape_set.exists():
            form = TrackNonstopForm(request.POST, instance=instance)
        else:
            form = TrackMetaForm(request.POST, instance=instance)
        form.save()
        new_zones = {x.title for x in instance.nonstop_zones.all()}
        if new_zones:
            if previous_zones == new_zones:
                pass
            elif previous_zones:
                logger.info(
                    'changed nonstop zones for track "%s" (%s) (from %s to %s)',
                    instance.title,
                    instance.id,
                    ', '.join(previous_zones),
                    ', '.join(new_zones),
                )
            else:
                logger.info(
                    'added track "%s" (%s) to nonstop (%s)', instance.title, instance.id, ', '.join(new_zones)
                )
                instance.added_to_nonstop_timestamp = now()
        else:
            if previous_zones:
                logger.info(
                    'removed track "%s" (%s) from nonstop (%s)',
                    instance.title,
                    instance.id,
                    ', '.join(previous_zones),
                )
            instance.added_to_nonstop_timestamp = None
        instance.save()
        return HttpResponseRedirect('.')


class TrackEditView(FormView):
    form_class = TrackArtistTitleForm
    template_name = 'nonstop/track_edit_artist_title.html'

    def dispatch(self, request, *args, **kwargs):
        self.track = Track.objects.get(id=kwargs['pk'])
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['object'] = self.track
        return ctx

    def get_initial(self):
        initial = {'artist': self.track.artist.name, 'title': self.track.title}
        if self.track.artist.name == app_settings.TRACK_UNKNOWN_ARTIST_LABEL:
            initial['artist'] = ''
        if self.track.title == app_settings.TRACK_UNKNOWN_TITLE_LABEL:
            initial['label'] = ''
        return initial

    def get_success_url(self):
        return reverse('track-view', kwargs=self.kwargs)

    def form_valid(self, form):
        assert self.request.user.has_perm('nonstop.add_track')
        changes = False
        current_artist_id = self.track.artist_id
        if self.track.artist.name != form.cleaned_data['artist']:
            self.track.artist, created = Artist.objects.get_or_create(name=form.cleaned_data['artist'])
            changes = True
        if self.track.title != form.cleaned_data['title']:
            self.track.title = form.cleaned_data['title']
            changes = True
        if changes:
            self.track.save()
            if (
                current_artist_id != self.track.artist_id
                and Track.objects.filter(artist_id=current_artist_id).count() == 0
            ):
                Artist.objects.filter(id=current_artist_id).delete()
            duplicate_tracks = Track.objects.filter(
                title=self.track.title, artist_id=self.track.artist_id
            ).exclude(id=self.track.id)
            if duplicate_tracks:
                if not self.track.file_exists():
                    try:
                        self.track.soundfiile = [x for x in duplicate_tracks if x.file_exists()][0].soundfile
                    except IndexError:
                        pass
                    else:
                        self.track.save()
                # remove others
                for track in duplicate_tracks:
                    SomaLogLine.objects.filter(track_id=track.id).update(track_id=self.track.id)
                    track.delete()
        return super().form_valid(form)


def track_sound(request, pk):
    try:
        track = Track.objects.get(id=pk)
    except Track.DoesNotExist:
        raise Http404()
    if not track.file_exists():
        raise Http404()
    file_path = track.file_path()
    remote_ip = (
        request.headers.get('x-forwarded-for')
        or request.headers.get('x-real-ip')
        or request.META.get('REMOTE_ADDR')
    )
    if remote_ip in settings.INTERNAL_IPS:
        # local user, always serve native file
        if track.soundfile and track.soundfile.url:
            return HttpResponseRedirect(track.soundfile.url)
        return FileResponse(open(file_path, 'rb'))
    # remote user, transcode
    cmdline = [
        'ffmpeg',
        '-loglevel',
        'quiet',
        '-y',
        '-i',
        file_path,
        '-f',
        'opus',
        '-b:a',
        '64000',
        '-',  # send to stdout
    ]
    if request.GET.get('short'):
        # serve a single minute if requested.
        cmdline[1:1] = ['-t', '60']
        if track.duration and track.duration.total_seconds() > 60:
            cmdline[1:1] = ['-ss', '60']
    cmd = subprocess.run(cmdline, capture_output=True)
    return HttpResponse(cmd.stdout, content_type='audio/opus')


class ArtistDetailView(DetailView):
    model = Artist


class ArtistListView(ListView):
    model = Artist

    def get_queryset(self):
        qs = super().get_queryset()
        if self.request.GET.get('q'):
            for part in self.request.GET.get('q').split():
                part = part.strip()
                if not part:
                    continue
                qs = qs.filter(name__icontains=part)

        zone = self.request.GET.get('zone')
        if zone:
            from emissions.models import Nonstop

            if zone == 'none':
                qs = qs.filter(track__nonstop_zones=None)
            elif zone == 'any':
                qs = qs.filter(track__nonstop_zones__isnull=False).distinct()
            elif zone == 'active':
                active_zones = []
                for z in Nonstop.objects.all():
                    if z.start != z.end:
                        active_zones.append(z.id)
                        zone_settings = z.nonstopzonesettings_set.all().first()
                        if zone_settings:
                            active_zones.extend([x['id'] for x in zone_settings.extra_zones.values('id')])
                qs = qs.filter(track__nonstop_zones__in=active_zones).distinct()
            else:
                qs = qs.filter(track__nonstop_zones=zone).distinct()

        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = ArtistSearchForm(self.request.GET)
        return context


class ArtistEditView(FormView):
    form_class = ArtistForm
    template_name = 'nonstop/artist_edit.html'

    def dispatch(self, request, *args, **kwargs):
        self.artist = Artist.objects.get(id=kwargs['pk'])
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['object'] = self.artist
        return ctx

    def get_initial(self):
        return {'name': self.artist.name}

    def get_success_url(self):
        return reverse('artist-view', kwargs={'pk': self.artist.id})

    def form_valid(self, form):
        assert self.request.user.has_perm('nonstop.add_track')
        if self.artist.name != form.cleaned_data['name']:
            existing_artist = Artist.objects.filter(name=form.cleaned_data['name']).first()
            if existing_artist:
                Track.objects.filter(artist_id=self.artist.id).update(artist_id=existing_artist.id)
                self.artist.delete()
                self.artist = existing_artist
            else:
                self.artist.name = form.cleaned_data['name']
                self.artist.save()
        return super().form_valid(form)


class JinglesListView(ListView):
    model = Jingle

    def get_queryset(self):
        return super().get_queryset().filter(emission__isnull=True)


class JingleAddView(CreateView):
    model = Jingle
    fields = ['label', 'soundfile']
    success_url = reverse_lazy('nonstop-jingles-list')


class JingleSettingsView(UpdateView):
    model = Jingle
    fields = [
        'label',
        'soundfile',
        'clock_time',
        'default_for_initial_diffusions',
        'default_for_reruns',
        'default_for_streams',
        'exclude_from_episodes',
    ]
    success_url = reverse_lazy('nonstop-jingles-list')


class ZoneAddView(CreateView):
    model = Nonstop
    template_name = 'nonstop/zone_add.html'
    fields = ('title',)

    def form_valid(self, form):
        if not self.request.user.has_perm('emissions.change_nonstop'):
            raise PermissionDenied()
        form.instance.start = datetime.time(0, 0)
        form.instance.end = datetime.time(0, 0)
        response = super().form_valid(form)
        NonstopZoneSettings.objects.create(nonstop=self.object)
        return response

    def get_success_url(self):
        return reverse('zone-settings', kwargs={'slug': self.object.slug})


class ZoneDeleteView(DeleteView):
    model = Nonstop
    template_name = 'nonstop/zone_confirm_delete.html'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm('emissions.change_nonstop'):
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('nonstop-zones')


class ZonesView(ListView):
    model = Nonstop
    template_name = 'nonstop/zones.html'

    def get_queryset(self):
        return sorted(
            super().get_queryset(), key=lambda x: datetime.time(23, 59) if (x.start == x.end) else x.start
        )


class ZoneStats:
    def __init__(self, zone, from_date=None, until_date=None, exclude_kwargs=None, **kwargs):
        self.zone = zone
        self.qs = Track.objects.all()
        if zone is not None:
            self.qs = self.qs.filter(nonstop_zones=self.zone)
        if kwargs:
            self.qs = self.qs.filter(**kwargs)
        if exclude_kwargs:
            self.qs = self.qs.exclude(**exclude_kwargs)
        self.from_date = from_date
        if from_date:
            self.qs = self.qs.filter(somalogline__play_timestamp__gte=from_date)
        self.until_date = until_date
        if until_date:
            self.qs = self.qs.filter(somalogline__play_timestamp__lte=until_date)
        self.qs = self.qs.distinct()

    def total_duration(self, **kwargs):
        try:
            total = self.qs.filter(**kwargs).aggregate(Sum('duration'))['duration__sum'].total_seconds()
        except AttributeError:
            # 'NoneType' object has no attribute 'total_seconds', if there's no
            # track in queryset
            return '-'
        if total > 3600 * 2:
            duration = _('%d hours') % (total / 3600)
        else:
            duration = _('%d minutes') % (total / 60)
        if self.zone:
            start = datetime.datetime(2000, 1, 1, self.zone.start.hour, self.zone.start.minute)
            end = datetime.datetime(2000, 1, 1, self.zone.end.hour, self.zone.end.minute)
            if end < start:
                end = end + datetime.timedelta(days=1)
            max_playtime = (end - start).total_seconds()
        else:
            max_playtime = 24 * 60 * 60
        return duration + _(', → %d days') % (total // max_playtime)

    def count(self, **kwargs):
        return self.qs.filter(**kwargs).count()

    def get_combined_stats(self):
        extra_zones = app_settings.EXTRA_ZONES.get(self.zone.slug) if self.zone else None
        if not extra_zones:
            return None
        return ZoneStats(
            zone=None,
            from_date=self.from_date,
            until_date=self.until_date,
            nonstop_zones__slug__in=[self.zone.slug] + app_settings.EXTRA_ZONES.get(self.zone.slug),
        )

    def percentage(self, **kwargs):
        total = self.count()
        if total == 0:
            return '-'
        percent = '%.2f%%' % (100.0 * self.count(**kwargs) / total)
        combined_stats = self.get_combined_stats()
        if not combined_stats:
            return percent
        combined_percent = combined_stats.percentage(**kwargs)
        return _('%(percent)s, with combined zones: %(combined_percent)s') % {
            'percent': percent,
            'combined_percent': combined_percent,
        }

    def instru(self):
        return self.count(instru=True)

    def instru_percentage(self):
        return self.percentage(instru=True)

    def sabam(self):
        return self.count(sabam=True)

    def sabam_percentage(self):
        return self.percentage(sabam=True)

    def cfwb(self):
        return self.count(cfwb=True)

    def cfwb_percentage(self):
        return self.percentage(cfwb=True)

    def language_set(self):
        return self.count() - self.language_unset()

    def language_unset(self):
        return self.count(language='')

    def unset_language_percentage(self):
        return self.percentage(language='')

    def french(self):
        return self.count(language='fr')

    def unset_or_na_language(self):
        return self.qs.filter(Q(language='') | Q(language='na')).count()

    def french_percentage(self):
        considered_tracks = self.count() - self.unset_or_na_language()
        if considered_tracks == 0:
            return '-'
        combined_stats = self.get_combined_stats()
        percent = '%.2f%%' % (100.0 * self.french() / considered_tracks)
        if not combined_stats:
            return percent

        combined_percent = combined_stats.french_percentage()
        return _('%(percent)s, with combined zones: %(combined_percent)s') % {
            'percent': percent,
            'combined_percent': combined_percent,
        }

    def quota_french(self):
        combined_stats = self.get_combined_stats()
        if not combined_stats:
            # no combined stats, just do stats on self
            combined_stats = self
        considered_tracks = combined_stats.count() - combined_stats.unset_or_na_language()
        if considered_tracks == 0:
            return True
        return (100.0 * combined_stats.french() / considered_tracks) > app_settings.FRENCH_QUOTA

    def quota_cfwb(self):
        combined_stats = self.get_combined_stats()
        if not combined_stats:
            # no combined stats, just do stats on self
            combined_stats = self
        considered_tracks = combined_stats.count()
        if considered_tracks == 0:
            return True
        return (100.0 * combined_stats.cfwb() / considered_tracks) > app_settings.CFWB_QUOTA

    def with_gender_metadata(self):
        return self.count(gender_m_computed_score__isnull=False)

    def with_gender_metadata_percentage(self):
        return self.percentage(gender_m_computed_score__isnull=False)

    def avg_gender_score(self, gender):
        value = (
            self.qs.exclude(gender_m_computed_score__isnull=True)
            .aggregate(Avg(f'gender_{gender}_computed_score'))
            .get(f'gender_{gender}_computed_score__avg')
        )
        if value is None:
            return '-'
        return int(100 * value)

    def avg_gender_m_score(self):
        return self.avg_gender_score('m')

    def avg_gender_f_score(self):
        return self.avg_gender_score('f')

    def avg_gender_x_score(self):
        return self.avg_gender_score('x')

    def new_files(self):
        return self.count(creation_timestamp__gte=self.from_date)

    def percent_new_files(self):
        return self.percentage(creation_timestamp__gte=self.from_date)


def parse_date(date):
    if date.endswith('d'):
        return datetime.datetime.today() + datetime.timedelta(int(date.rstrip('d')))
    return datetime.datetime.strptime(date, '%Y-%m-%d').date()


class StatisticsView(TemplateView):
    template_name = 'nonstop/statistics.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['include_sabam_meta'] = app_settings.INCLUDE_SABAM_META
        context['include_cfwb_meta'] = app_settings.INCLUDE_CFWB_META
        context['include_gender_meta'] = app_settings.INCLUDE_GENDER_META
        context['zones'] = [x for x in Nonstop.objects.all().order_by('start') if x.start != x.end]
        kwargs = {}
        if 'from' in self.request.GET:
            kwargs['from_date'] = parse_date(self.request.GET['from'])
            context['from_date'] = kwargs['from_date']
        if 'until' in self.request.GET:
            kwargs['until_date'] = parse_date(self.request.GET['until'])
        if 'onair' in self.request.GET:
            kwargs['somalogline__on_air'] = True
        for zone in context['zones']:
            zone.stats = ZoneStats(zone, **kwargs)

        class GlobalScheduled:
            title = _('Global (scheduled)')
            start = None
            end = None
            stats = ZoneStats(zone=None, nonstop_zones__isnull=False, **kwargs)

        class GlobalScheduledOrNot:
            title = _('Global (scheduled or not)')
            start = None
            end = None
            stats = ZoneStats(zone=None, **kwargs)

        context['zones'].append(GlobalScheduled())
        context['zones'].append(GlobalScheduledOrNot())

        context['french_quota'] = app_settings.FRENCH_QUOTA
        context['cfwb_quota'] = app_settings.CFWB_QUOTA
        return context


class UploadTracksView(FormView):
    form_class = UploadTracksForm
    template_name = 'nonstop/upload.html'
    success_url = '.'

    def post(self, request, *args, **kwargs):
        assert self.request.user.has_perm('nonstop.add_track')
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        tracks = request.FILES.getlist('tracks')
        if not form.is_valid():
            return self.form_invalid(form)
        missing_metadata = []
        invalid_files = []
        metadatas = {}
        for f in tracks:
            with tempfile.NamedTemporaryFile(prefix='track-upload') as tmpfile:
                tmpfile.write(f.read())
                f.seek(0)
                try:
                    metadata = mutagen.File(tmpfile.name, easy=True)
                except Exception as e:
                    logger.info('error reading file (%s)' % e)
                    invalid_files.append(f.name)
                    continue
            if not metadata or not metadata.get('artist') or not metadata.get('title'):
                if app_settings.TRACK_UNKNOWN_TITLE_LABEL and app_settings.TRACK_UNKNOWN_ARTIST_LABEL:
                    metadatas[f.name] = {
                        'title': [app_settings.TRACK_UNKNOWN_TITLE_LABEL],
                        'artist': [app_settings.TRACK_UNKNOWN_ARTIST_LABEL],
                    }
                else:
                    missing_metadata.append(f.name)
            else:
                metadatas[f.name] = metadata
        if missing_metadata or invalid_files:
            error_messages = []
            if missing_metadata:
                error_messages.append(_('Missing metadata in: ') + ', '.join(missing_metadata))
            if invalid_files:
                error_messages.append(_('Unreadable metadata in: ') + ', '.join(invalid_files))
            form.add_error('tracks', ' / '.join([str(x) for x in error_messages]))
            return self.form_invalid(form)

        for f in tracks:
            metadata = metadatas[f.name]
            artist_name = metadata.get('artist')[0]
            track_title = metadata.get('title')[0]

            monthdir = datetime.datetime.today().strftime('%Y-%m')
            filepath = '%s/%s - %s - %s%s' % (
                monthdir,
                datetime.datetime.today().strftime('%y%m%d'),
                artist_name[:50].replace('/', ' ').strip(),
                track_title[:80].replace('/', ' ').strip(),
                os.path.splitext(f.name)[-1],
            )

            artist = Artist.objects.filter(name__iexact=artist_name).first()
            if artist:
                created = False
            else:
                artist, created = Artist.objects.get_or_create(name=artist_name)
            if track_title == app_settings.TRACK_UNKNOWN_TITLE_LABEL:
                # always create a new track for unknown titles
                track = Track.objects.create(
                    title=track_title,
                    artist=artist,
                    uploader=self.request.user,
                )
                created = True
            else:
                track = Track.objects.filter(title__iexact=track_title, artist=artist).first()
                if track:
                    created = False
                else:
                    track, created = Track.objects.get_or_create(
                        title=track_title, artist=artist, defaults={'uploader': self.request.user}
                    )
            if created or not track.file_exists():
                logger.info('uploaded track "%s" (%s)', track.title, track.id)
                track.soundfile = default_storage.save(os.path.join('nonstop', 'tracks', filepath), content=f)
                track.save()
            else:
                # don't keep duplicated file and do not create a duplicated nonstop file object
                pass
            if request.POST.get('nonstop_zone'):
                nonstop_zone = Nonstop.objects.get(id=request.POST.get('nonstop_zone'))
                track.nonstop_zones.add(nonstop_zone)
                logger.info('added track "%s" (%s) to nonstop (%s)', track.title, track.id, nonstop_zone)
                if not track.added_to_nonstop_timestamp:
                    track.added_to_nonstop_timestamp = now()
                track.save()

        messages.info(self.request, '%d new track(s)' % len(tracks))
        return self.form_valid(form)


class TracksMetadataView(ListView):
    template_name = 'nonstop/tracks_metadata.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['view'] = self
        context['include_sabam_meta'] = app_settings.INCLUDE_SABAM_META
        context['include_cfwb_meta'] = app_settings.INCLUDE_CFWB_META
        context['include_gender_meta'] = app_settings.INCLUDE_GENDER_META
        return context

    def get_queryset(self):
        return Track.objects.exclude(nonstop_zones__isnull=True)

    def post(self, request, *args, **kwargs):
        assert self.request.user.has_perm('nonstop.add_track')
        for track_id in request.POST.getlist('track'):
            track = Track.objects.get(id=track_id)
            track.language = request.POST.get('lang-%s' % track_id, '')
            track.instru = 'instru-%s' % track_id in request.POST
            if app_settings.INCLUDE_SABAM_META:
                track.sabam = 'sabam-%s' % track_id in request.POST
            if app_settings.INCLUDE_CFWB_META:
                track.cfwb = 'cfwb-%s' % track_id in request.POST
            if app_settings.INCLUDE_GENDER_META:
                track.gender_m = request.POST.get('gender-m-%s' % track_id)
                track.gender_m = int(track.gender_m) if track.gender_m else None
                track.gender_f = request.POST.get('gender-f-%s' % track_id)
                track.gender_f = int(track.gender_f) if track.gender_f else None
                track.gender_x = request.POST.get('gender-x-%s' % track_id)
                track.gender_x = int(track.gender_x) if track.gender_x else None
                track.gender_l = request.POST.get('gender-l-%s' % track_id)
            track.save()
        return HttpResponseRedirect('.')


class RandomTracksMetadataView(TracksMetadataView):
    page_title = _('Metadata of random tracks')

    def get_queryset(self):
        if super().get_queryset().filter(Q(language='') | Q(language__isnull=True)).exists():
            # if there are tracks without language metadata, get from them
            qs = super().get_queryset().filter(Q(language='') | Q(language__isnull=True))
        else:
            # otherwise, get from all tracks
            qs = super().get_queryset()
        return qs.order_by('?')[:50]


class RandomFillGenderTracksMetadataView(TracksMetadataView):
    page_title = _('Metadata of random tracks with no gender metadata')

    def get_queryset(self):
        qs = (
            super()
            .get_queryset()
            .exclude(gender_m__isnull=False)
            .exclude(gender_f__isnull=False)
            .exclude(gender_x__isnull=False)
        )
        if not qs.exists():
            # get from all tracks
            qs = super().get_queryset()
        return qs.order_by('?')[:50]


class RecentTracksMetadataView(TracksMetadataView):
    page_title = _('Metadata of recent tracks')

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['include_pagination'] = True
        ctx['pagination_next'] = int(self.request.GET.get('page') or 0) + 1
        return ctx

    def get_queryset(self):
        page = int(self.request.GET.get('page') or 0)
        return (
            super()
            .get_queryset()
            .exclude(creation_timestamp__isnull=True)
            .order_by('-creation_timestamp')[page * 50 : (page + 1) * 50]
        )

    def post(self, request, *args, **kwargs):
        super().post(request, *args, **kwargs)
        return HttpResponseRedirect('./?page=%s' % int(self.request.GET.get('page') or 0))


class ArtistTracksMetadataView(TracksMetadataView):
    @property
    def page_title(self):
        return _('Metadata of tracks from %s') % Artist.objects.get(id=self.kwargs['artist_pk']).name

    def get_queryset(self):
        return Track.objects.filter(artist_id=self.kwargs['artist_pk']).order_by('title')


class QuickLinksView(TemplateView):
    template_name = 'nonstop/quick_links.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        day = datetime.datetime.today()
        context['days'] = [day + datetime.timedelta(days=i) for i in range(5)]
        context['has_switch'] = bool(app_settings.ON_AIR_SWITCH_URL)
        return context


class SearchView(TemplateView):
    template_name = 'nonstop/search.html'

    @classmethod
    def get_search_queryset(cls, q=None, zone=None):
        queryset = Track.objects.all()

        if q:
            queryset = queryset.filter(Q(title__icontains=q.lower()) | Q(artist__name__icontains=q.lower()))

        if zone:
            from emissions.models import Nonstop

            if zone == 'none':
                queryset = queryset.filter(nonstop_zones=None)
            elif zone == 'any':
                queryset = queryset.filter(nonstop_zones__isnull=False).distinct()
            elif zone == 'active':
                active_zones = []
                for z in Nonstop.objects.all():
                    if z.start != z.end:
                        active_zones.append(z.id)
                        zone_settings = z.nonstopzonesettings_set.all().first()
                        if zone_settings:
                            active_zones.extend([x['id'] for x in zone_settings.extra_zones.values('id')])
                queryset = queryset.filter(nonstop_zones__in=active_zones).distinct()
            else:
                queryset = queryset.filter(nonstop_zones=zone).distinct()

        return queryset

    def get_queryset(self):
        queryset = self.get_search_queryset(q=self.request.GET.get('q'), zone=self.request.GET.get('zone'))
        order = self.request.GET.get('order_by') or 'title'
        if order:
            if 'added_to_nonstop_timestamp' in order:
                queryset = queryset.filter(added_to_nonstop_timestamp__isnull=False)
            queryset = queryset.order_by(order)
        return queryset

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['form'] = TrackSearchForm(self.request.GET)
        queryset = self.get_queryset()
        qs = self.request.GET.copy()
        qs.pop('page', None)
        ctx['qs'] = qs.urlencode()

        tracks = Paginator(queryset.select_related(), 20)

        page = self.request.GET.get('page')
        try:
            ctx['tracks'] = tracks.page(page)
        except PageNotAnInteger:
            ctx['tracks'] = tracks.page(1)
        except EmptyPage:
            ctx['tracks'] = tracks.page(tracks.num_pages)

        return ctx


class SearchCsvView(SearchView):
    def get(self, request, *args, **kwargs):
        out = StringIO()
        writer = csv.writer(out)
        columns = [
            _('Title'),
            _('Artist'),
            _('Zones'),
            _('Language'),
            _('Instru'),
        ]
        if app_settings.INCLUDE_CFWB_META:
            columns.append('CFWB')
        columns.append('Ajout')
        if app_settings.INCLUDE_GENDER_META:
            columns.append(_('(cis)Male members'))
            columns.append(_('(cis)Female members'))
            columns.append(_('Non-binary, trans or agender members'))
            columns.append(_('Lead'))
            columns.append('%M')
            columns.append('%F')
            columns.append('%NB')
        writer.writerow(columns)
        for track in self.get_queryset():
            row = [
                track.title if track.title else 'Inconnu',
                track.artist.name if (track.artist and track.artist.name) else 'Inconnu',
                ' + '.join([x.title for x in track.nonstop_zones.all()]),
                track.language or '',
                track.instru and 'instru' or '',
            ]
            if app_settings.INCLUDE_CFWB_META:
                row.append(track.cfwb and 'cfwb' or '')
            row.append(
                track.added_to_nonstop_timestamp.strftime('%Y-%m-%d %H:%M')
                if track.added_to_nonstop_timestamp
                else ''
            )
            if app_settings.INCLUDE_GENDER_META:
                row.append('' if track.gender_m is None else str(track.gender_m))
                row.append('' if track.gender_f is None else str(track.gender_f))
                row.append('' if track.gender_x is None else str(track.gender_x))
                row.append('' if track.gender_l is None else str(track.gender_l))
                row.append(
                    ''
                    if track.gender_m_computed_score is None
                    else '%.1f' % (100 * track.gender_m_computed_score)
                )
                row.append(
                    ''
                    if track.gender_f_computed_score is None
                    else '%.1f' % (100 * track.gender_f_computed_score)
                )
                row.append(
                    ''
                    if track.gender_x_computed_score is None
                    else '%.1f' % (100 * track.gender_x_computed_score)
                )

            writer.writerow(row)
        return HttpResponse(out.getvalue(), content_type='text/csv; charset=utf-8')


class TracksAddToZoneView(FormView):
    form_class = TracksAddToZoneForm
    template_name = 'nonstop/search-add-to-zone.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        qs = self.request.GET.copy()
        ctx['qs'] = qs.urlencode()
        return ctx

    def form_valid(self, form):
        assert self.request.user.has_perm('nonstop.add_track')
        add_to_zone = Nonstop.objects.get(id=form.cleaned_data['zone'])
        query_string = dict(urllib.parse.parse_qs(self.request.POST['qs']))
        q = query_string['q'][0] if query_string.get('q') else None
        zone = query_string['zone'][0] if query_string.get('zone') else None
        for track in SearchView.get_search_queryset(q=q, zone=zone):
            track.nonstop_zones.add(add_to_zone)
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('nonstop-search') + '?' + self.request.POST['qs']


class CleanupView(TemplateView):
    template_name = 'nonstop/cleanup.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['form'] = CleanupForm(
            initial={
                'zone': int(self.request.GET.get('zone')) if self.request.GET.get('zone') else None,
                'no_metadata': bool(self.request.GET.get('no_metadata') == 'on'),
            }
        )

        zone = self.request.GET.get('zone')
        if zone:
            from emissions.models import Nonstop

            ctx['zone'] = Nonstop.objects.get(id=zone)
            qs = Track.objects.filter(nonstop_zones=zone)
            if self.request.GET.get('no_metadata') == 'on':
                qs = qs.filter(Q(language='') | Q(language__isnull=True))
            ctx['count'] = qs.count()
            ctx['tracks'] = qs.order_by('added_to_nonstop_timestamp').select_related()[:30]
        return ctx

    def post(self, request, *args, **kwargs):
        assert self.request.user.has_perm('nonstop.add_track')
        count = 0
        for track_id in request.POST.getlist('track'):
            if request.POST.get('remove-%s' % track_id):
                track = Track.objects.get(id=track_id)
                track.nonstop_zones.clear()
                logger.info('cleaned up track "%s" (%s) from nonstop', track.title, track.id)
                count += 1
        if count:
            messages.info(self.request, 'Removed %d new track(s)' % count)
        return HttpResponseRedirect('.')


class AddSomaDiffusionView(CreateView):
    model = ScheduledDiffusion
    fields = ['jingle', 'stream']
    template_name = 'nonstop/streamed-diffusion.html'

    @property
    def fields(self):
        diffusion = Diffusion.objects.get(id=self.kwargs['pk'])
        fields = ['jingle']
        if not diffusion.episode.soundfile_set.filter(fragment=False).exists():
            fields.append('stream')
        return fields

    def get_form_class(self, **kwargs):
        form_class = super().get_form_class()
        form_class.base_fields['jingle'].queryset = Jingle.objects.filter(clock_time__isnull=True).exclude(
            exclude_from_episodes=True
        )
        return form_class

    def get_initial(self):
        initial = super().get_initial()
        initial['jingle'] = None
        if 'stream' in self.fields:
            initial['jingle'] = Jingle.objects.filter(default_for_streams=True).order_by('?').first()
        initial['stream'] = Stream.objects.all().first()
        return initial

    def form_valid(self, form):
        form.instance.diffusion_id = self.kwargs['pk']
        episode = form.instance.diffusion.episode
        if 'stream' in self.fields and form.instance.stream_id is None:
            messages.error(self.request, _('missing stream'))
            return HttpResponseRedirect(
                reverse('episode-view', kwargs={'emission_slug': episode.emission.slug, 'slug': episode.slug})
            )
        response = super().form_valid(form)
        logger.info('registered diffusion (%s)', form.instance.diffusion)
        messages.info(self.request, _('%s added to schedule') % episode.emission.title)
        return response

    def get_success_url(self):
        diffusion = Diffusion.objects.get(id=self.kwargs['pk'])
        episode = diffusion.episode
        return reverse('episode-view', kwargs={'emission_slug': episode.emission.slug, 'slug': episode.slug})


class DelSomaDiffusionView(RedirectView):
    def get_redirect_url(self, pk):
        soma_diffusion = ScheduledDiffusion.objects.filter(diffusion_id=pk).first()
        if soma_diffusion is None:
            raise Http404()
        logger.info('removed diffusion (%s)', soma_diffusion.diffusion)
        episode = soma_diffusion.episode
        ScheduledDiffusion.objects.filter(diffusion_id=pk).update(diffusion_id=None)
        messages.info(self.request, _('%s removed from schedule') % episode.emission.title)
        return reverse('episode-view', kwargs={'emission_slug': episode.emission.slug, 'slug': episode.slug})


class DiffusionPropertiesView(UpdateView):
    model = ScheduledDiffusion
    fields = ['jingle', 'stream']
    template_name = 'nonstop/streamed-diffusion.html'

    @property
    def fields(self):
        diffusion = self.get_object().diffusion
        fields = ['jingle']
        if not diffusion.episode.soundfile_set.filter(fragment=False).exists():
            fields.append('stream')
        return fields

    def get_form_class(self, **kwargs):
        form_class = super().get_form_class()
        form_class.base_fields['jingle'].queryset = Jingle.objects.filter(clock_time__isnull=True)
        return form_class

    def form_valid(self, form):
        episode = self.get_object().diffusion.episode
        if 'stream' in self.fields and form.instance.stream_id is None:
            messages.error(self.request, _('missing stream'))
            return HttpResponseRedirect(
                reverse('episode-view', kwargs={'emission_slug': episode.emission.slug, 'slug': episode.slug})
            )
        response = super().form_valid(form)
        messages.info(self.request, _('%s diffusion properties have been updated.') % episode.emission.title)
        return response

    def get_success_url(self):
        episode = self.get_object().diffusion.episode
        return reverse('episode-view', kwargs={'emission_slug': episode.emission.slug, 'slug': episode.slug})


def jingle_audio_view(request, *args, **kwargs):
    jingle = Jingle.objects.get(id=kwargs['pk'])
    try:
        jingle_path = jingle.soundfile.path
    except (AttributeError, ValueError):
        raise Http404()

    cmdline = [
        'ffmpeg',
        '-loglevel',
        'quiet',
        '-y',
        '-i',
        jingle_path,
        '-f',
        'opus',
        '-b:a',
        '64000',
        '-',  # send to stdout
    ]
    cmd = subprocess.run(cmdline, capture_output=True)
    return HttpResponse(cmd.stdout, content_type='audio/opus')


class AjaxProgram(TemplateView):
    template_name = 'nonstop/program-fragment.html'

    def get_context_data(self, date, **kwargs):
        context = super().get_context_data(**kwargs)
        now = datetime.datetime.now()
        if date:
            date_start = datetime.datetime.strptime(date, '%Y-%m-%d')
        else:
            date_start = datetime.datetime.today()
        date_start = date_start.replace(hour=5, minute=0, second=0, microsecond=0)
        today = bool(date_start.timetuple()[:3] == now.timetuple()[:3])
        date_end = date_start + datetime.timedelta(days=1)
        context['day_program'] = period_program(date_start, date_end, prefetch_categories=False)
        for x in context['day_program']:
            x.klass = x.__class__.__name__
        previous_prog = None
        for i, x in enumerate(context['day_program']):
            if today and x.datetime > now and previous_prog:
                previous_prog.now = True
                break
            previous_prog = x
        return context


class ZoneSettings(FormView):
    form_class = ZoneSettingsForm
    template_name = 'nonstop/zone_settings.html'
    success_url = reverse_lazy('nonstop-zones')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['zone'] = Nonstop.objects.get(slug=self.kwargs['slug'])
        return context

    def get_initial(self):
        try:
            zone = Nonstop.objects.get(slug=self.kwargs['slug'])
        except Nonstop.DoesNotExist:
            raise Http404()
        zone_settings = zone.nonstopzonesettings_set.first()
        if zone_settings is None:
            zone_settings = NonstopZoneSettings(nonstop=zone)
            zone_settings.save()
        initial = super().get_initial()
        initial['start'] = zone.start.strftime('%H:%M') if zone.start else None
        initial['end'] = zone.end.strftime('%H:%M') if zone.end else None
        initial['intro_jingle'] = zone_settings.intro_jingle_id
        initial['jingles'] = [x.id for x in zone_settings.jingles.all()]
        initial['extra_zones'] = [x.id for x in zone_settings.extra_zones.all()]
        for key, value in zone_settings.weights.items():
            initial['weight_%s' % key] = value
        return initial

    def form_valid(self, form):
        if not self.request.user.has_perm('emissions.change_nonstop'):
            raise PermissionDenied()
        zone = Nonstop.objects.get(slug=self.kwargs['slug'])
        zone_settings = zone.nonstopzonesettings_set.first()
        zone.start = form.cleaned_data['start']
        zone.end = form.cleaned_data['end']
        zone_settings.jingles.set(form.cleaned_data['jingles'])
        zone_settings.extra_zones.set(form.cleaned_data['extra_zones'])
        zone_settings.intro_jingle_id = form.cleaned_data['intro_jingle']
        weights = {key[7:]: value for key, value in form.cleaned_data.items() if key.startswith('weight_')}
        zone_settings.weights = weights
        zone.save()
        zone_settings.save()
        logger.info('updated nonstop settings for %s', zone)
        return super().form_valid(form)


class ZoneEditorialSettings(UpdateView):
    model = Nonstop
    fields = [
        'title',
        'subtitle',
        'text',
        'image',
        'image_attribution_url',
        'image_attribution_text',
        'redirect_path',
    ]
    success_url = reverse_lazy('nonstop-zones')
    template_name = 'nonstop/zone_editorial_settings.html'


class MuninTracks(StatisticsView):
    template_name = 'nonstop/munin_tracks.txt'
    content_type = 'text/plain; charset=utf-8'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['nonstop_general_total'] = Track.objects.count()
        active_tracks_qs = Track.objects.filter(nonstop_zones__isnull=False).distinct()
        context['nonstop_general_active'] = active_tracks_qs.count()
        context['nonstop_percentages_instru'] = 100 * (
            active_tracks_qs.filter(instru=True).count() / context['nonstop_general_active']
        )
        context['nonstop_percentages_cfwb'] = 100 * (
            active_tracks_qs.filter(cfwb=True).count() / context['nonstop_general_active']
        )
        context['nonstop_percentages_langset'] = 100 * (
            active_tracks_qs.exclude(language='').count() / context['nonstop_general_active']
        )
        context['nonstop_percentages_french'] = 100 * (
            active_tracks_qs.filter(language='fr').count()
            / active_tracks_qs.exclude(language__isnull=True).exclude(language__in=('', 'na')).count()
        )
        return context


class ZoneTracklistPercents(DetailView):
    model = Nonstop

    def get(self, request, *args, **kwargs):
        zone = self.get_object()
        zone_settings = zone.nonstopzonesettings_set.first()
        weights = {key[7:]: int(value) for key, value in request.GET.items() if key.startswith('weight_')}
        zone_settings.weights = weights

        zone_ids = [zone.id]
        zone_ids.extend([x.id for x in zone_settings.extra_zones.all()])

        tracklist = utils.Tracklist(zone_settings, zone_ids=zone_ids, requires_metadata=True)
        random_tracks_iterator = tracklist.get_random_tracks(k=100)

        counts = collections.defaultdict(int)

        for i, track in enumerate(random_tracks_iterator):
            if i == 1000:
                break
            for weight_key, weight_value in weights.items():
                if track.match_criteria(weight_key, weight_value):
                    counts[weight_key] += 1

        data = {}
        for weight in weights:
            data[weight] = counts[weight] / 1000

        # second percentage for French tracks quota
        data['french_quota'] = counts['lang_fr'] / (1000 - counts['instru'])

        return JsonResponse(data)


class RecurringView(DetailView):
    template_name = 'nonstop/recurring_index.html'
    model = Emission

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object'] = self.get_object()
        context['recurring'] = (
            RecurringStreamDiffusion.objects.filter(schedule__in=self.get_object().schedule_set.all()).first()
            or RecurringPlaylistDiffusion.objects.filter(
                schedule__in=self.get_object().schedule_set.all()
            ).first()
        )
        return context


class RecurringAddStreamView(FormView):
    form_class = RecurringStreamForm
    template_name = 'nonstop/recurring_form.html'

    def dispatch(self, request, *args, **kwargs):
        self.emission = Emission.objects.get(slug=kwargs['slug'])
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        for schedule in self.emission.schedule_set.filter(rerun=False):
            RecurringStreamDiffusion.objects.create(
                schedule=schedule,
                stream_id=form.cleaned_data['stream'],
                jingle_id=form.cleaned_data['jingle'] or None,
            )
        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        return reverse('nonstop-recurring-stuff', kwargs={'slug': self.emission.slug})


class RecurringAddPlaylistView(FormView):
    form_class = RecurringPlaylistAddForm
    template_name = 'nonstop/recurring_form.html'

    def dispatch(self, request, *args, **kwargs):
        self.emission = Emission.objects.get(slug=kwargs['slug'])
        self.recurrence = RecurringPlaylistDiffusion.objects.filter(
            schedule__in=self.emission.schedule_set.all()
        ).first()
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        weights = {key[7:]: value for key, value in form.cleaned_data.items() if key.startswith('weight_')}
        zones_set = [Nonstop.objects.get(id=x) for x in form.cleaned_data['zones']]

        for schedule in self.emission.schedule_set.filter(rerun=False):
            rec, created = RecurringPlaylistDiffusion.objects.get_or_create(schedule=schedule)
            rec.jingle_id = form.cleaned_data['jingle']
            if weights:
                rec.weights = weights
            rec.save()
            rec.zones.set(zones_set)
        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        return reverse('nonstop-recurring-stuff', kwargs={'slug': self.emission.slug})


class RecurringEditPlaylistView(RecurringAddPlaylistView):
    form_class = RecurringPlaylistEditForm

    def get_initial(self):
        initial = super().get_initial()
        initial['jingle'] = self.recurrence.jingle_id
        initial['zones'] = [x.id for x in self.recurrence.zones.all()]
        for key, value in self.recurrence.weights.items():
            initial['weight_%s' % key] = value
        return initial


class RecurringDeleteView(FormView):
    form_class = RecurringDeleteForm
    template_name = 'nonstop/recurring_form.html'

    def dispatch(self, request, *args, **kwargs):
        self.emission = Emission.objects.get(slug=kwargs['slug'])
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        RecurringStreamDiffusion.objects.filter(schedule__emission=self.emission).delete()
        RecurringPlaylistDiffusion.objects.filter(schedule__emission=self.emission).delete()
        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        return reverse('nonstop-recurring-stuff', kwargs={'slug': self.emission.slug})


class StreamsListView(ListView):
    model = Stream


class StreamAddView(CreateView):
    model = Stream
    fields = ['label', 'url']
    success_url = reverse_lazy('nonstop-streams')

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm('nonstop.change_stream'):
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)


class StreamEditView(UpdateView):
    model = Stream
    fields = ['label', 'url']
    success_url = reverse_lazy('nonstop-streams')

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm('nonstop.change_stream'):
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)


class StreamDeleteView(DeleteView):
    model = Stream
    success_url = reverse_lazy('nonstop-streams')

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm('nonstop.change_stream'):
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)


class TrackToTapeView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        Tape.objects.get_or_create(full_track_id=kwargs['pk'])
        return reverse('track-view', kwargs=self.kwargs)


class TapeTrackAddView(FormView):
    form_class = TapeTrackForm
    template_name = 'nonstop/tape_track_add.html'

    def dispatch(self, request, *args, **kwargs):
        self.tape = Tape.objects.get(id=kwargs['pk'])
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['tape'] = self.tape
        return ctx

    def get_initial(self):
        return {'sabam': True}

    def get_success_url(self):
        return reverse('track-view', kwargs={'pk': self.tape.full_track.id})

    def form_valid(self, form):
        assert self.request.user.has_perm('nonstop.add_track')
        position_parts = [int(x) for x in form.cleaned_data['position'].split(':')]
        position = datetime.timedelta(minutes=position_parts[0], seconds=position_parts[1])
        artist, created = Artist.objects.get_or_create(name=form.cleaned_data['artist'])
        track, created = Track.objects.get_or_create(artist=artist, title=form.cleaned_data['title'])
        track.instru = form.cleaned_data.get('instru')
        track.language = form.cleaned_data.get('language')
        if app_settings.INCLUDE_SABAM_META:
            track.sabam = form.cleaned_data.get('sabam')
        if app_settings.INCLUDE_CFWB_META:
            track.cfwb = form.cleaned_data.get('cfwb')
        if app_settings.INCLUDE_GENDER_META:
            track.gender_m = form.cleaned_data.get('gender_m')
            track.gender_f = form.cleaned_data.get('gender_f')
            track.gender_x = form.cleaned_data.get('gender_x')
            track.gender_l = form.cleaned_data.get('gender_l')
        track.save()
        TapePart.objects.create(tape=self.tape, track=track, position=position)
        return super().form_valid(form)


class TapePartEditView(FormView):
    form_class = TapeTrackEditForm
    template_name = 'nonstop/tape_track_add.html'

    def dispatch(self, request, *args, **kwargs):
        self.tape_part = TapePart.objects.get(id=kwargs['pk'])
        self.tape = self.tape_part.tape
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['tape'] = self.tape_part.tape
        ctx['tape_part'] = self.tape_part
        return ctx

    def get_initial(self):
        return {
            'artist': self.tape_part.track.artist.name,
            'title': self.tape_part.track.title,
            'position': '%02d:%02d'
            % (self.tape_part.position.seconds / 60, self.tape_part.position.seconds % 60),
        }

    def get_success_url(self):
        return reverse('track-view', kwargs={'pk': self.tape.full_track.id})

    def form_valid(self, form):
        assert self.request.user.has_perm('nonstop.add_track')
        position_parts = [int(x) for x in form.cleaned_data['position'].split(':')]
        position = datetime.timedelta(minutes=position_parts[0], seconds=position_parts[1])
        artist, created = Artist.objects.get_or_create(name=form.cleaned_data['artist'])
        track, created = Track.objects.get_or_create(artist=artist, title=form.cleaned_data['title'])
        self.tape_part.track = track
        self.tape_part.position = position
        self.tape_part.save()
        return super().form_valid(form)


class TapePartRemoveView(DeleteView):
    model = TapePart
    template_name = 'nonstop/tape_part_confirm_delete.html'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm('nonstop.add_track'):
            raise PermissionDenied()
        self.tape = self.get_object().tape
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('track-view', kwargs={'pk': self.tape.full_track.id})


def force_switch_view(request, *args, **kwargs):
    logger.info('forced switch back to nonstop')
    messages.info(request, _('Forced back switch to nonstop'))
    utils.switch_audio_source(0)
    return HttpResponseRedirect(reverse('nonstop-quick-links'))

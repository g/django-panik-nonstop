from django.forms.widgets import ClearableFileInput, NumberInput, SelectMultiple, TimeInput
from django.utils.safestring import mark_safe

from .models import Jingle


class TimeWidget(TimeInput):
    input_type = 'time'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.attrs['step'] = '300'  # 5 minutes
        self.attrs['pattern'] = '[0-9]{2}:[0-9]{2}'


class MulticheckWidget(SelectMultiple):
    def render(self, name, value, attrs=None, choices=(), renderer=None):
        s = []
        value = value or []
        for choice_id, choice_label in choices or self.choices:
            s.append(
                '<li><label><input type="checkbox" '
                '  name="%(name)s-%(choice_id)s" %(checked)s>'
                '<span>%(choice_label)s</span></label></li>'
                % {
                    'name': name,
                    'checked': 'checked' if choice_id in value else '',
                    'choice_id': choice_id,
                    'choice_label': choice_label,
                }
            )
        return mark_safe('<ul id="%(id)s">' % attrs + '\n'.join(s) + '</ul>')

    def value_from_datadict(self, data, files, name):
        choices = []
        for choice_id, choice_label in self.choices:
            if data.get('%s-%s' % (name, choice_id)):
                choices.append(str(choice_id))
        return choices


class RangeWidget(NumberInput):
    input_type = 'range'
    template_name = 'nonstop/range_widget.html'


class MultipleClearableFileInput(ClearableFileInput):
    allow_multiple_selected = True

import datetime
import json
import logging
import os
import random
import shutil
import socket
import time
import xml.etree.ElementTree as ET

import requests
from django.template import loader
from django.utils.module_loading import import_string
from django.utils.timezone import now
from emissions.models import Diffusion, Schedule
from requests.exceptions import RequestException

from .app_settings import app_settings
from .models import Jingle, RecurringOccurence, ScheduledDiffusion, SomaLogLine, Track


class SomaException(Exception):
    pass


def get_current_nonstop_track(content_filter=None):
    soma_log_line = SomaLogLine.objects.filter(track__tape__isnull=True, play_timestamp__lte=now())
    if content_filter:
        soma_log_line = import_string(content_filter)()['nonstop_track_filter'](soma_log_line)
    soma_log_line = soma_log_line.select_related().order_by('-play_timestamp').first()

    if not soma_log_line:
        # nothing yet
        return {}

    current_track = soma_log_line.track
    if current_track is None:
        # nonstop is playing a nonstop track, but it's unknown :/
        return {}

    if current_track and current_track.duration:
        expired_time = soma_log_line.play_timestamp + current_track.duration
    else:
        expired_time = soma_log_line.play_timestamp + datetime.timedelta(hours=1)
    if datetime.datetime.now() > expired_time:
        # last known line is way too old
        return {}
    d = {'track_title': current_track.title}
    if current_track.artist:
        d['track_artist'] = current_track.artist.name
    return d


def get_diffusion_file_path(diffusion):
    return 'diffusions-auto/%s--%s' % (
        diffusion.datetime.strftime('%Y%m%d-%H%M'),
        diffusion.episode.emission.slug,
    )


def is_already_in_soma(diffusion):
    if isinstance(diffusion, Diffusion):
        if ScheduledDiffusion.objects.filter(diffusion=diffusion).exists():
            return True
    elif isinstance(diffusion, Schedule):
        if RecurringOccurence.objects.filter(stream_diffusion__schedule=diffusion).exists():
            return True
        if RecurringOccurence.objects.filter(directory_diffusion__schedule=diffusion).exists():
            return True
        if RecurringOccurence.objects.filter(playlist_diffusion__schedule=diffusion).exists():
            return True
    return False


def soma_connection():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(('soma', 12521))
    s.recv(1024)  # -> b'soma 2.5 NO_SSL\n'
    s.sendall(b'100 - Ok\n')
    s.recv(1024)  # -> b'100 - Welcome to soma daemon\n'
    s.sendall(b'\n')  # (empty password)
    if s.recv(1024) != b'100 - Ok\n':
        raise SomaException('failed to initialize soma connection')
    return s


def add_soma_diffusion(diffusion):
    context = {}
    context['diffusion'] = diffusion
    context['jingle'] = diffusion.jingle
    context['episode'] = diffusion.episode
    context['start'] = diffusion.datetime
    context['end'] = diffusion.end_datetime

    if not diffusion.is_stream():
        # program a soundfile
        soundfile = diffusion.episode.soundfile_set.filter(fragment=False)[0]
        diffusion_path = get_diffusion_file_path(diffusion)

        # copy file
        if not os.path.exists(app_settings.LOCAL_BASE_PATH):
            raise SomaException('soma directory is not available')
        local_diffusion_path = os.path.join(app_settings.LOCAL_BASE_PATH, diffusion_path)
        if os.path.exists(local_diffusion_path):
            for filename in os.listdir(local_diffusion_path):
                os.unlink(os.path.join(local_diffusion_path, filename))
        else:
            os.mkdir(os.path.join(app_settings.LOCAL_BASE_PATH, diffusion_path))
        try:
            shutil.copyfile(
                soundfile.file.path,
                os.path.join(
                    app_settings.LOCAL_BASE_PATH, diffusion_path, os.path.basename(soundfile.file.path)
                ),
            )
        except OSError:
            try:
                os.rmdir(os.path.join(app_settings.LOCAL_BASE_PATH, diffusion_path))
            except OSError:
                pass
            raise SomaException('error copying file to soma')

        context['diffusion_path'] = diffusion_path
        # end should be a bit before the real end of file so the same file doesn't
        # get repeated but shouldn't be less or equal than start date or soma would
        # loop the file
        context['end'] = diffusion.datetime + datetime.timedelta(
            seconds=max(((soundfile.duration or 480) - 180), 60)
        )

    # create palinsesti
    palinsesti_template = loader.get_template('nonstop/soma_palinsesti.xml')

    palinsesti = palinsesti_template.render(context)
    palinsesti_xml = ET.fromstring(palinsesti.encode('utf-8'))

    palinsesto_xml = get_palinsesto_xml()
    palinsesto_xml.append(palinsesti_xml)
    send_palinsesto_xml(palinsesto_xml)
    diffusion.added_to_nonstop_timestamp = now()
    diffusion.save()


def remove_soma_diffusion(diffusion):
    palinsesto_xml = get_palinsesto_xml()
    for palinsesto in palinsesto_xml.findall('Palinsesto'):
        if palinsesto.findall('Description')[0].text.startswith(diffusion.soma_id):
            palinsesto_xml.remove(palinsesto)
    send_palinsesto_xml(palinsesto_xml)


def send_palinsesto_xml(palinsesto_xml):
    with soma_connection() as s:
        s.sendall(b'106 - Switch to a New Palinsesto Request\n')
        if s.recv(1024) != b'100 - Ok\n':
            raise SomaException('failed to switch palinsesto')
        s.sendall(ET.tostring(palinsesto_xml))

    # give it some time (...)
    time.sleep(3)
    with soma_connection() as s:
        s.sendall(b'122 - Set the current Palinsesto as Default\n')
        if s.recv(1024) != b'100 - Ok\n':
            raise SomaException('failed to set current palinsesto as default')


def get_palinsesto_xml():
    with soma_connection() as s:
        s.sendall(b'109 - Get the current palinsesto\n')
        palinsesto_bytes = b''
        while True:
            new_bytes = s.recv(200000)
            if not new_bytes:
                break
            palinsesto_bytes += new_bytes
        if not palinsesto_bytes.startswith(b'100 - Ok\n'):
            raise SomaException('failed to get palinsesto')
        palinsesto_bytes = palinsesto_bytes[9:]
    palinsesto_xml = ET.fromstring(palinsesto_bytes)
    return palinsesto_xml


class Tracklist:
    def __init__(
        self,
        zone_settings,
        zone_ids,
        recent_tracks_id=None,
        recent_artists_id=None,
        requires_metadata=False,
        k=30,
        logger=None,
    ):
        self.zone_settings = zone_settings
        self.zone_ids = zone_ids
        self.playlist = []
        self.recent_tracks_id = recent_tracks_id or []
        self.recent_artists_id = recent_artists_id or []
        self.requires_metadata = requires_metadata
        self.k = k
        self.ignore_recent_tracks = bool(Track.objects.filter(soundfile__isnull=False).count() < 250)
        self.logger = logger or logging.getLogger('panikdb')

    def append(self, track):
        # track or jingle
        self.playlist.append(track)

    def pop(self):
        return self.playlist.pop() if self.playlist else None

    def get_recent_track_ids(self):
        if self.ignore_recent_tracks:
            return []
        return self.recent_tracks_id + [x.id for x in self.playlist if isinstance(x, Track)]

    def get_recent_artist_ids(self):
        if self.ignore_recent_tracks:
            return []
        return self.recent_artists_id + [x.artist_id for x in self.playlist if isinstance(x, Track)]

    def get_duration(self):
        duration = datetime.timedelta(seconds=0)
        for track in self.playlist:
            duration += track.duration
            # add some arbitrary elapsed time to account for player starting & loading track
            duration += datetime.timedelta(seconds=app_settings.PLAYER_DURATION_DRIFT)
        return duration

    def get_random_tracks(self, k=30, extra_filters=None):
        weights = self.zone_settings.weights

        # custom behaviour when all language weights are -10 but one, consider
        # that as requiring only tracks from that language
        lang_weights = ('lang_en', 'lang_fr', 'lang_nl', 'lang_other', 'instru')
        picked_ten = None
        count_minus_ten = 0
        for weight_attribute in lang_weights:
            weight = weights.get(weight_attribute)
            if weight == 10:
                picked_ten = weight_attribute
            elif weight == -10:
                count_minus_ten += 1

        extra_filters = extra_filters or {}
        if picked_ten and count_minus_ten == 4:
            weights[picked_ten] = 0  # neutralize effect wrt other weights
            if picked_ten == 'instru':
                picked_ten = 'lang_na'
            extra_filters['language'] = picked_ten.split('_')[-1]

        while True:
            # pick tracks from db
            tracks = Track.objects.filter(nonstop_zones__in=self.zone_ids, soundfile__isnull=False)
            if self.requires_metadata:
                tracks = tracks.exclude(language__isnull=True).exclude(language='')
            if extra_filters:
                tracks = tracks.filter(**extra_filters)
            tracks = (
                tracks.exclude(id__in=self.get_recent_track_ids())
                .exclude(artist_id__in=self.get_recent_artist_ids())
                .order_by('?')[: k * 10]
            )
            if len(tracks) == 0:
                if len(self.recent_tracks_id) == 0 or k == 1:
                    # abort if there's no recent tracks list, or we've been asked for a single track.
                    # (typically to fill a very specific time slot, as we do not want the fallback to
                    # go on with no known recent tracks)
                    break
                # cut recent lists in half, hopefully this will help getting enough tracks
                self.logger.warning(
                    'Not enough tracks/artists, ignoring recent restrictions '
                    '(%s recent tracks, %s recent artists) (extra: %r)',
                    len(self.recent_tracks_id),
                    len(self.recent_artists_id),
                    extra_filters,
                )
                self.recent_tracks_id = self.recent_tracks_id[: len(self.recent_tracks_id) // 2]
                self.recent_artists_id = self.recent_artists_id[: len(self.recent_artists_id) // 2]
                continue

            def compute_weight(track):
                weight = 0
                for weight_key, weight_value in weights.items():
                    if track.match_criteria(weight_key, weight_value):
                        weight += weight_value
                if weight < 0:
                    weight = 1 + (weight / 20)
                else:
                    weight = 1 + (weight / 2)
                return weight

            track_weights = [compute_weight(x) for x in tracks]
            tracks = random.choices(tracks, weights=track_weights, k=k)

            seen = set()
            for track in tracks:
                if track in seen:
                    continue
                yield track
                seen.add(track)


def switch_audio_source(source):
    if app_settings.JACK_SWITCH_SEND_UDP_NOTIFICATIONS:
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
        sock.sendto(
            json.dumps({'active': int(source)}).encode(),
            app_settings.JACK_SWITCH_SEND_UDP_NOTIFICATIONS,
        )
    if app_settings.ON_AIR_SWITCH_URL:
        try:
            requests.post(app_settings.ON_AIR_SWITCH_URL, data={'s': str(source)}, timeout=2)
        except RequestException as e:
            logger = logging.getLogger('panikdb')
            logger.warning('failed to notify switch using HTTP (%s)', e)

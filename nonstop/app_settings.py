import os

from django.conf import settings


class AppSettings:
    def get_setting(self, setting, default=None):
        return getattr(settings, 'NONSTOP_' + setting, default)

    @property
    def REMOTE_BASE_PATH(self):
        return self.get_setting('REMOTE_BASE_PATH', os.path.join(settings.MEDIA_ROOT, 'nonstop/'))

    @property
    def LOCAL_BASE_PATH(self):
        return self.get_setting('LOCAL_BASE_PATH', os.path.join(settings.MEDIA_ROOT, 'nonstop/'))

    @property
    def JINGLES_PREFIX(self):
        # relative to ..._BASE_PATH
        return self.get_setting('JINGLES_PREFIX', 'SPOTS')

    @property
    def PLAYER_COMMAND(self):
        return self.get_setting('PLAYER_COMMAND', 'mpv')

    @property
    def PLAYER_ARGS(self):
        return self.get_setting('PLAYER_ARGS', [])

    @property
    def PLAYER_IPC_PATH(self):
        return self.get_setting('PLAYER_IPC_PATH', '')

    @property
    def PLAYER_DURATION_DRIFT(self):
        # track duration drift caused by player starting, loading track, cleaning up afterwards, etc.
        return self.get_setting('PLAYER_DURATION_DRIFT', 0)

    @property
    def MUMBLE_PLAYER_COMMAND(self):
        return self.get_setting('MUMBLE_PLAYER_COMMAND')

    @property
    def ON_AIR_SWITCH_URL(self):
        return self.get_setting('ON_AIR_SWITCH_URL', None)

    @property
    def JACK_SWITCH_SEND_UDP_NOTIFICATIONS(self):
        # tuple with (ip, port) (or None to skip notifications)
        return self.get_setting('JACK_SWITCH_SEND_UDP_NOTIFICATIONS', None)

    @property
    def NO_REPEAT_DELAY(self):
        # in seconds (0 to disable)
        return self.get_setting('NO_REPEAT_DELAY', 7 * 24 * 3600)

    @property
    def NO_REPEAT_ARTIST_DELAY(self):
        # in seconds (0 to disable)
        return self.get_setting('NO_REPEAT_ARTIST_DELAY', 2 * 3600)

    @property
    def AUTO_SCHEDULE(self):
        return self.get_setting('AUTO_SCHEDULE', False)

    @property
    def SERVER_BIND_IFACE(self):
        return self.get_setting('SERVER_BIND_IFACE', '127.0.0.1')

    @property
    def SERVER_BIND_PORT(self):
        return self.get_setting('SERVER_BIND_PORT', 8888)

    @property
    def EXTRA_ZONES(self):
        # zone slug to list of zone slugs,
        # ex: {"le-mange-disque": ['hop-bop-and-co', 'up-beat-tempo']}
        # to get tracks from additional zones.
        return self.get_setting('EXTRA_ZONES', {})

    @property
    def FRENCH_QUOTA(self):
        # obligation de diffuser annuellement au moins X% d'œuvres musicales de
        # langue française
        return self.get_setting('FRENCH_QUOTA', 0)

    @property
    def CFWB_QUOTA(self):
        # obligation de diffuser annuellement au moins N% d'œuvres musicales
        # émanant de la Communauté française.
        return self.get_setting('CFWB_QUOTA', 0)

    @property
    def MAX_JINGLE_DURATION(self):
        return self.get_setting('MAX_JINGLE_DURATION', 60)

    @property
    def INCLUDE_SABAM_META(self):
        return self.get_setting('INCLUDE_SABAM_META', False)

    @property
    def INCLUDE_CFWB_META(self):
        return self.get_setting('INCLUDE_CFWB_META', False)

    @property
    def INCLUDE_GENDER_META(self):
        return self.get_setting('INCLUDE_GENDER_META', False)

    @property
    def ENABLE_TAPE_MODE(self):
        return self.get_setting('ENABLE_TAPE_MODE', False)

    @property
    def SWITCH_WS_URL(self):
        # example: 'wss://panikdb.radiopanik.org/ws/switch/'
        return self.get_setting('SWITCH_WS_URL', None)

    @property
    def SWITCH_IN_PORTS(self):
        # example: (keys are source IDs)
        # {
        #  0: ('netjack_soma:capture_1', 'netjack_soma:capture_2'),
        #  1: ('alsa_in:capture_1', 'alsa_in:capture_2'),
        # }
        return self.get_setting('SWITCH_IN_PORTS', {})

    @property
    def SWITCH_OUT_PORTS(self):
        # example: (keys are irrelevant)
        #  {
        #   'audio': ('alsa_out:playback_1', 'alsa_out:playback_2'),
        #   'stream': ('liquidsoap:in_0', 'liquidsoap:in_1'),
        #  }
        return self.get_setting('SWITCH_OUT_PORTS', {})

    @property
    def TRACK_UNKNOWN_TITLE_LABEL(self):
        # label to use when a track is missing a title metadata
        # (None to require metadata)
        return self.get_setting('TRACK_UNKNOWN_TITLE_LABEL', None)

    @property
    def TRACK_UNKNOWN_ARTIST_LABEL(self):
        # label to use when a track is missing an artist metadata
        # (None to require metadata)
        return self.get_setting('TRACK_UNKNOWN_ARTIST_LABEL', None)


app_settings = AppSettings()

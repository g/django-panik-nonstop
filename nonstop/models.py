import collections
import datetime
import json
import os
import random

from django.conf import settings
from django.core.files.storage import default_storage
from django.db import models
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _
from emissions.utils import get_duration

from .app_settings import app_settings


class Artist(models.Model):
    name = models.CharField(_('Name'), max_length=255)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('artist-view', kwargs={'pk': self.id})

    def recent_diffusions(self):
        return (
            SomaLogLine.objects.filter(track__artist=self)
            .exclude(on_air=False)
            .order_by('-play_timestamp')
            .select_related('track')
            .select_related('track__artist')
        )

    def active_tracks(self):
        return self.track_set.filter(nonstop_zones__isnull=False).distinct().order_by('title')

    def available_tracks(self):
        return self.track_set.filter(nonstop_zones__isnull=True).order_by('title')


class Album(models.Model):
    name = models.CharField(_('Name'), max_length=255)


LANGUAGES = [
    ('en', _('English')),
    ('fr', _('French')),
    ('nl', _('Dutch')),
    ('other', _('Other')),
    ('na', _('Not applicable')),
]


class Track(models.Model):
    title = models.CharField(_('Title'), max_length=255)
    artist = models.ForeignKey(Artist, null=True, on_delete=models.SET_NULL)
    album = models.ForeignKey(Album, null=True, on_delete=models.SET_NULL)
    instru = models.BooleanField(_('Instru'), default=False)
    language = models.CharField(_('Language'), max_length=10, choices=LANGUAGES, blank=True)
    sabam = models.BooleanField('SABAM', default=True)
    cfwb = models.BooleanField('CFWB', default=False)

    # gender metadata
    gender_m = models.PositiveSmallIntegerField(_('(cis)Male members'), blank=True, null=True)
    gender_f = models.PositiveSmallIntegerField(_('(cis)Female members'), blank=True, null=True)
    gender_x = models.PositiveSmallIntegerField(
        _('Non-binary, trans or agender members'), blank=True, null=True
    )
    gender_l = models.CharField(
        _('Lead'),
        choices=[
            ('m', _('Male')),
            ('f', ('Female')),
            ('x', _('Non-binary, trans or agender')),
        ],
        blank=True,
        null=True,
    )
    gender_m_computed_score = models.FloatField(blank=True, null=True)
    gender_f_computed_score = models.FloatField(blank=True, null=True)
    gender_x_computed_score = models.FloatField(blank=True, null=True)

    nonstop_zones = models.ManyToManyField('emissions.Nonstop', blank=True)

    creation_timestamp = models.DateTimeField(auto_now_add=True, null=True)
    added_to_nonstop_timestamp = models.DateTimeField(null=True)
    uploader = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)

    soundfile = models.FileField(
        _('File'), upload_to='nonstop/tracks/%Y-%m', max_length=255, null=True, blank=True
    )
    duration = models.DurationField(_('Duration'), null=True)

    class Meta:
        ordering = ['creation_timestamp']

    def __str__(self):
        return 'Track %s (%s)' % (self.title, self.artist or 'unknown')

    def save(self, *args, **kwargs):
        self.gender_m_computed_score = None
        self.gender_f_computed_score = None
        self.gender_x_computed_score = None
        total = (self.gender_m or 0) + (self.gender_f or 0) + (self.gender_x or 0)
        if total:
            self.gender_m_computed_score = (self.gender_m or 0) / total
            self.gender_f_computed_score = (self.gender_f or 0) / total
            self.gender_x_computed_score = (self.gender_x or 0) / total
            if self.gender_l and self.gender_l != 'N':  # lead
                self.gender_m_computed_score /= 2
                self.gender_f_computed_score /= 2
                self.gender_x_computed_score /= 2
                if self.gender_l == 'M':
                    self.gender_m_computed_score += 0.5
                if self.gender_l == 'F':
                    self.gender_f_computed_score += 0.5
                if self.gender_l == 'X':
                    self.gender_x_computed_score += 0.5
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('track-view', kwargs={'pk': self.id})

    def recent_diffusions(self):
        return (
            SomaLogLine.objects.filter(track=self)
            .exclude(on_air=False)
            .order_by('-play_timestamp')
            .select_related('track')
            .select_related('track__artist')
        )

    def file_path(self):
        if self.soundfile and self.soundfile.path:
            return self.soundfile.path
        return None

    def file_exists(self):
        file_path = self.file_path()
        if not file_path:
            return False
        try:
            return os.path.exists(file_path)
        except AttributeError:
            return False

    def match_criteria(self, criteria, weight_value):
        return getattr(self, 'match_criteria_' + criteria)()

    def match_criteria_lang_en(self):
        return self.language == 'en'

    def match_criteria_lang_fr(self):
        return self.language == 'fr'

    def match_criteria_lang_nl(self):
        return self.language == 'nl'

    def match_criteria_lang_other(self):
        return self.language == 'other'

    def match_criteria_instru(self):
        return self.instru

    def match_criteria_cfwb(self):
        return self.cfwb

    def match_criteria_newest(self):
        if not self.added_to_nonstop_timestamp:
            return False
        now = datetime.datetime.now()
        return (now - self.added_to_nonstop_timestamp).days < 30

    def match_criteria_new(self):
        if not self.added_to_nonstop_timestamp:
            return False
        now = datetime.datetime.now()
        return (now - self.added_to_nonstop_timestamp).days < 100


class Tape(models.Model):
    # single file referencing real tracks
    full_track = models.ForeignKey(Track, on_delete=models.CASCADE)
    parts = models.ManyToManyField(Track, through='TapePart', related_name='tapes')

    def get_parts(self):
        return TapePart.objects.filter(tape=self.id).order_by('position')


class TapePart(models.Model):
    tape = models.ForeignKey(Tape, on_delete=models.CASCADE)
    track = models.ForeignKey(Track, on_delete=models.CASCADE)
    position = models.DurationField(_('Position'), null=True, blank=True)

    def __str__(self):
        return f'{self.track} @ {self.position} in {self.tape.full_track.title}'


class SomaLogLine(models.Model):
    class Meta:
        verbose_name = _('Soma log line')
        verbose_name_plural = _('Soma log lines')
        ordering = ['play_timestamp']

    track = models.ForeignKey(Track, null=True, on_delete=models.SET_NULL)
    play_timestamp = models.DateTimeField()
    on_air = models.BooleanField('On Air', null=True)
    channel = models.CharField('Channel', max_length=255, blank=True, null=True)


class Jingle(models.Model):
    class Meta:
        verbose_name = _('Jingle')
        verbose_name_plural = _('Jingles')
        ordering = ['label']

    label = models.CharField(_('Label'), max_length=100)
    filepath = models.CharField(_('File Path'), max_length=255, blank=True)
    soundfile = models.FileField(
        _('File'), upload_to='nonstop/jingles/', max_length=255, null=True, blank=True
    )
    duration = models.DurationField(_('Duration'), null=True, blank=True)
    clock_time = models.TimeField(_('Clock Time'), null=True, blank=True)
    default_for_initial_diffusions = models.BooleanField(_('Default for initial diffusions'), default=False)
    default_for_reruns = models.BooleanField(_('Default for reruns'), default=False)
    default_for_streams = models.BooleanField(_('Default for streams'), default=False)
    emission = models.ForeignKey('emissions.Emission', null=True, blank=True, on_delete=models.SET_NULL)
    exclude_from_episodes = models.BooleanField(_('Exclude from episodes selection'), default=False)

    def __str__(self):
        if self.duration:
            return '%s (%ds)' % (self.label, self.duration.total_seconds())
        return self.label

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if self.soundfile:
            self.duration = datetime.timedelta(seconds=float(get_duration(self.get_local_filepath())))
            super().save(*args, **kwargs)

    @property
    def short(self):
        # property for compatibility with Track model
        # for jingles self.filepath is actually only the last part of the path,
        # ex: jingles panik/H_marimba_RP_chucho_zoe.wav
        if self.soundfile:
            return os.path.split(self.soundfile.name)[-1]
        return self.filepath

    def file_path(self):
        # for compatibility with Track model method
        return self.get_local_filepath()

    def get_local_filepath(self):
        if not self.short:
            return None
        if self.soundfile:
            return default_storage.path(self.soundfile.name)
        return os.path.join(app_settings.LOCAL_BASE_PATH, app_settings.JINGLES_PREFIX, self.short)

    @property
    def title(self):
        return self.label


class Stream(models.Model):
    class Meta:
        verbose_name = _('Stream')
        verbose_name_plural = _('Streams')
        ordering = ['label']

    label = models.CharField(_('Label'), max_length=100)
    url = models.URLField(_('URL'), max_length=255)

    def __str__(self):
        return self.label


class ScheduledDiffusion(models.Model):
    class Meta:
        verbose_name = _('Scheduled diffusion')
        verbose_name_plural = _('Scheduled diffusions')

    diffusion = models.ForeignKey('emissions.Diffusion', null=True, blank=True, on_delete=models.CASCADE)
    jingle = models.ForeignKey(Jingle, null=True, blank=True, on_delete=models.SET_NULL)
    stream = models.ForeignKey(Stream, null=True, blank=True, on_delete=models.SET_NULL)
    creation_timestamp = models.DateTimeField(auto_now_add=True, null=True)
    added_to_nonstop_timestamp = models.DateTimeField(null=True)
    auto_delayed = models.BooleanField(default=False)

    def __str__(self):
        return 'Diffusion of %s' % self.diffusion

    @property
    def datetime(self):
        return self.diffusion.datetime

    @property
    def end_datetime(self):
        if self.is_stream():
            return self.diffusion.end_datetime
        dt = self.diffusion.datetime
        if self.jingle and self.jingle.duration:
            dt += self.jingle.duration
        dt += datetime.timedelta(seconds=self.soundfile.duration)
        return dt

    @property
    def soundfile(self):
        # get main soundfile, and order by podcastable in case there are multiple
        # non fragment files, so that the file not marked as podcastable is selected.
        return self.diffusion.episode.soundfile_set.filter(fragment=False).order_by('podcastable').first()

    @property
    def duration(self):
        return (self.end_datetime - self.datetime).seconds

    @property
    def episode(self):
        return self.diffusion.episode

    @property
    def soma_id(self):
        if self.is_stream():
            return '[stream:%s]' % self.id
        else:
            return '[sound:%s]' % self.id

    def is_stream(self):
        return bool(self.stream_id)

    def get_jingle_filepath(self):
        return self.jingle.get_local_filepath() if self.jingle_id else None

    def file_path(self):
        # TODO, copy and stuff
        return self.soundfile.file.path


class NonstopZoneSettings(models.Model):
    nonstop = models.ForeignKey('emissions.Nonstop', on_delete=models.CASCADE)
    intro_jingle = models.ForeignKey(
        Jingle, blank=True, null=True, related_name='+', on_delete=models.SET_NULL
    )
    jingles = models.ManyToManyField(Jingle, blank=True)
    extra_zones = models.ManyToManyField('emissions.Nonstop', blank=True, related_name='extra_zones')

    weights_text = models.TextField(null=True)  # will be read as json

    def __str__(self):
        return str(self.nonstop)

    @property
    def weights(self):
        weights = collections.defaultdict(int)
        for k, v in json.loads(self.weights_text or "{}").items():
            weights[k] = v
        return weights

    @weights.setter
    def weights(self, d):
        self.weights_text = json.dumps(d)


class RecurringStreamDiffusion(models.Model):
    schedule = models.ForeignKey('emissions.Schedule', on_delete=models.CASCADE)
    jingle = models.ForeignKey(Jingle, null=True, blank=True, on_delete=models.SET_NULL)
    stream = models.ForeignKey(Stream, on_delete=models.CASCADE)
    is_active = models.BooleanField('Active', default=True)

    def __str__(self):
        return _('Recurring Stream for %s') % self.schedule

    @property
    def description(self):
        return _('stream "%s"') % self.stream.label


class RecurringOccurenceMixin:
    @property
    def jingle(self):
        return self.diffusion.jingle

    @property
    def jingle_id(self):
        return self.diffusion.jingle_id

    @property
    def duration(self):
        return self.diffusion.schedule.get_duration() * 60

    @property
    def end_datetime(self):
        return self.datetime + datetime.timedelta(minutes=self.diffusion.schedule.get_duration())

    def get_jingle_filepath(self):
        return self.diffusion.jingle.get_local_filepath() if self.diffusion.jingle_id else None


class RecurringRandomDirectoryDiffusion(models.Model):
    # between soundfiles and nonstop zones, this is used for the "mix
    # deliveries" segment during weekend nights.
    schedule = models.ForeignKey('emissions.Schedule', on_delete=models.CASCADE)
    jingle = models.ForeignKey(Jingle, null=True, blank=True, on_delete=models.SET_NULL)
    directory = models.CharField(_('Directory'), max_length=255)
    is_active = models.BooleanField('Active', default=True)

    def __str__(self):
        return _('Recurring Random Directory for %s') % self.schedule


class RecurringPlaylistDiffusion(models.Model):
    # autogenerate playlist from nonstop zones
    schedule = models.ForeignKey('emissions.Schedule', on_delete=models.CASCADE)
    jingle = models.ForeignKey(Jingle, null=True, blank=True, on_delete=models.SET_NULL)
    is_active = models.BooleanField('Active', default=True)
    zones = models.ManyToManyField('emissions.Nonstop', blank=True, related_name='recurring_playlist_zones')
    weights_text = models.TextField(null=True)  # will be read as json

    def __str__(self):
        return _('Playlist for %s') % self.schedule

    @property
    def intro_jingle(self):
        return self.jingle

    @property
    def weights(self):
        weights = collections.defaultdict(int)
        for k, v in json.loads(self.weights_text or "{}").items():
            weights[k] = v
        return weights

    @weights.setter
    def weights(self, d):
        self.weights_text = json.dumps(d)

    @property
    def description(self):
        return _('tracks from %s') % ', '.join([x.title for x in self.zones.all()])


class RecurringOccurence(models.Model, RecurringOccurenceMixin):
    stream_diffusion = models.ForeignKey(RecurringStreamDiffusion, on_delete=models.CASCADE, null=True)
    directory_diffusion = models.ForeignKey(
        RecurringRandomDirectoryDiffusion, on_delete=models.CASCADE, null=True
    )
    playlist_diffusion = models.ForeignKey(RecurringPlaylistDiffusion, on_delete=models.CASCADE, null=True)
    datetime = models.DateTimeField(_('Date/time'), db_index=True)

    def __str__(self):
        return 'Recurring occurence of %s' % (
            self.stream_diffusion.stream if self.stream_diffusion else self.diffusion
        )

    @property
    def diffusion(self):
        return self.stream_diffusion or self.directory_diffusion or self.playlist_diffusion

    @property
    def stream(self):
        return self.diffusion.stream

    @property
    def playlist_slot(self):
        return bool(self.playlist_diffusion)

    def get_as_nonstop_slot(self):
        # fake a nonstop object, to be used in stamina to generate playlist
        slot = self.playlist_diffusion
        slot.start = self.datetime.time()
        slot.end_datetime = self.datetime + datetime.timedelta(
            minutes=self.playlist_diffusion.schedule.get_duration()
        )
        return slot

    def is_stream(self):
        return bool(self.stream_diffusion)

    def file_path(self):
        directory = self.diffusion.directory
        return os.path.join(directory, random.choice(os.listdir(directory)))


@receiver(post_delete)
def remove_soundfile(sender, instance=None, **kwargs):
    from emissions.models import SoundFile

    if not issubclass(sender, SoundFile):
        return
    if not instance.episode.soundfile_set.filter(fragment=False).exists():
        ScheduledDiffusion.objects.filter(diffusion__episode_id=instance.episode_id, stream_id=None).update(
            diffusion=None
        )


@receiver(post_save)
def save_soundfile(sender, instance=None, **kwargs):
    if not app_settings.AUTO_SCHEDULE:
        return
    from emissions.models import SoundFile

    if not issubclass(sender, SoundFile):
        return
    if instance.fragment:
        return
    if not instance.duration:
        return
    for i, diffusion in enumerate(instance.episode.diffusion_set.all()):
        if diffusion.datetime < datetime.datetime.now():
            continue
        if i == 0:
            jingle_qs = Jingle.objects.filter(default_for_initial_diffusions=True)
        else:
            jingle_qs = Jingle.objects.filter(default_for_reruns=True)
        ScheduledDiffusion.objects.get_or_create(
            diffusion=diffusion, stream_id=None, defaults={'jingle': jingle_qs.order_by('?').first()}
        )


@receiver(post_save)
def save_diffusion(sender, instance=None, **kwargs):
    if not app_settings.AUTO_SCHEDULE:
        return
    from emissions.models import Diffusion

    if not issubclass(sender, Diffusion):
        return
    if instance.datetime < datetime.datetime.now():
        return
    if instance.episode.soundfile_set.filter(fragment=False).exists():
        if Diffusion.objects.filter(episode=instance.episode, datetime__lt=instance.datetime).exists():
            # rerun
            jingle_qs = Jingle.objects.filter(default_for_reruns=True)
        else:
            jingle_qs = Jingle.objects.filter(default_for_initial_diffusions=True)
        ScheduledDiffusion.objects.get_or_create(
            diffusion=instance, stream_id=None, defaults={'jingle': jingle_qs.order_by('?').first()}
        )


@receiver(post_save)
def save_schedule(sender, instance=None, **kwargs):
    # keep recurring content synced for all schedules of an emission
    from emissions.models import Schedule

    if not issubclass(sender, Schedule):
        return
    if instance.rerun:
        return
    stream_diffusion = RecurringStreamDiffusion.objects.filter(
        schedule__emission_id=instance.emission_id
    ).first()
    if stream_diffusion:
        RecurringStreamDiffusion.objects.get_or_create(
            schedule=instance,
            jingle=stream_diffusion.jingle,
            stream=stream_diffusion.stream,
            is_active=stream_diffusion.is_active,
        )
        return

    playlist_diffusion = RecurringPlaylistDiffusion.objects.filter(
        schedule__emission_id=instance.emission_id
    ).first()
    if playlist_diffusion:
        new_diffusion, created = RecurringPlaylistDiffusion.objects.get_or_create(
            schedule=instance,
            jingle=playlist_diffusion.jingle,
            is_active=playlist_diffusion.is_active,
            defaults={'weights_text': playlist_diffusion.weights_text},
        )
        if created:
            new_diffusion.zones.set(playlist_diffusion.zones.all())

from django.template import Library

from .. import utils

register = Library()


@register.filter
def is_already_in_soma(diffusion):
    return utils.is_already_in_soma(diffusion)


@register.filter
def somafix(value):
    return value.replace('&', 'and')


@register.filter
def integral(queryset):
    return queryset.exclude(fragment=True)

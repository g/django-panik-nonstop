from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('nonstop', '0009_track_added_to_nonstop_timestamp'),
    ]

    operations = [
        migrations.AddField(
            model_name='track',
            name='duration',
            field=models.DurationField(null=True, verbose_name='Duration'),
        ),
    ]

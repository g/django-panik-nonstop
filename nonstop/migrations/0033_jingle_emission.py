# Generated by Django 1.11.29 on 2020-09-06 16:11

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('emissions', '0015_auto_20200404_1510'),
        ('nonstop', '0032_auto_20200905_1348'),
    ]

    operations = [
        migrations.AddField(
            model_name='jingle',
            name='emission',
            field=models.ForeignKey(
                blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='emissions.Emission'
            ),
        ),
    ]

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('nonstop', '0005_nonstopfile_filename'),
    ]

    operations = [
        migrations.AlterField(
            model_name='nonstopfile',
            name='filename',
            field=models.CharField(max_length=255, null=True, verbose_name='Filename'),
        ),
    ]

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('nonstop', '0003_track_nonstop_zones'),
    ]

    operations = [
        migrations.AlterField(
            model_name='track',
            name='nonstop_zones',
            field=models.ManyToManyField(to='emissions.Nonstop', blank=True),
        ),
    ]

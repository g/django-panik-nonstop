from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('nonstop', '0006_auto_20171002_2231'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='track',
            options={'ordering': ['creation_timestamp']},
        ),
        migrations.AddField(
            model_name='track',
            name='creation_timestamp',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]

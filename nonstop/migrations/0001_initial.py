from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = []

    operations = [
        migrations.CreateModel(
            name='Album',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Artist',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NonstopFile',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('filepath', models.CharField(max_length=255, verbose_name='Filepath')),
                ('creation_timestamp', models.DateTimeField(auto_now_add=True, null=True)),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SomaLogLine',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('play_timestamp', models.DateTimeField()),
                ('filepath', models.ForeignKey(to='nonstop.NonstopFile', on_delete=models.SET_NULL)),
            ],
            options={
                'ordering': ['play_timestamp'],
                'verbose_name': 'Soma log line',
                'verbose_name_plural': 'Soma log lines',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Track',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=255, verbose_name='Title')),
                ('instru', models.BooleanField(default=False, verbose_name='Instru')),
                (
                    'language',
                    models.CharField(
                        blank=True,
                        max_length=3,
                        choices=[(b'en', 'English'), (b'fr', 'French'), (b'nl', 'Dutch')],
                    ),
                ),
                ('sabam', models.BooleanField(default=True, verbose_name=b'SABAM')),
                ('cfwb', models.BooleanField(default=False, verbose_name=b'CFWB')),
                ('album', models.ForeignKey(to='nonstop.Album', null=True, on_delete=models.SET_NULL)),
                ('artist', models.ForeignKey(to='nonstop.Artist', null=True, on_delete=models.SET_NULL)),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='nonstopfile',
            name='track',
            field=models.ForeignKey(to='nonstop.Track', null=True, on_delete=models.SET_NULL),
            preserve_default=True,
        ),
    ]

# Generated by Django 2.2.28 on 2022-07-16 13:06

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('nonstop', '0039_auto_20220716_1253'),
    ]

    operations = [
        migrations.AddField(
            model_name='recurringoccurence',
            name='directory_diffusion',
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to='nonstop.RecurringRandomDirectoryDiffusion',
            ),
        ),
        migrations.AlterField(
            model_name='recurringoccurence',
            name='stream_diffusion',
            field=models.ForeignKey(
                null=True, on_delete=django.db.models.deletion.CASCADE, to='nonstop.RecurringStreamDiffusion'
            ),
        ),
        migrations.DeleteModel(
            name='RecurringRandomDirectoryOccurence',
        ),
    ]

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('emissions', '0003_newsitem_event_date'),
        ('nonstop', '0002_somalogline_on_air'),
    ]

    operations = [
        migrations.AddField(
            model_name='track',
            name='nonstop_zones',
            field=models.ManyToManyField(to='emissions.Nonstop', null=True, blank=True),
            preserve_default=True,
        ),
    ]

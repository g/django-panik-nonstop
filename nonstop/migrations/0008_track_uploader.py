from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('nonstop', '0007_auto_20171003_1022'),
    ]

    operations = [
        migrations.AddField(
            model_name='track',
            name='uploader',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL),
        ),
    ]

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('nonstop', '0008_track_uploader'),
    ]

    operations = [
        migrations.AddField(
            model_name='track',
            name='added_to_nonstop_timestamp',
            field=models.DateTimeField(null=True),
        ),
    ]

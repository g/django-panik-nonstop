import os

from django.db import migrations, models


def create_filenames(apps, schema_editor):
    NonstopFile = apps.get_model('nonstop', 'NonstopFile')
    for nonstop_file in NonstopFile.objects.filter(filename__isnull=True):
        nonstop_file.filename = os.path.basename(nonstop_file.filepath)
        nonstop_file.save()


class Migration(migrations.Migration):
    dependencies = [
        ('nonstop', '0004_auto_20170418_1525'),
    ]

    operations = [
        migrations.AddField(
            model_name='nonstopfile',
            name='filename',
            field=models.CharField(max_length=255, null=True, verbose_name='Filename', unique=True),
        ),
        migrations.RunPython(create_filenames, lambda x, y: None),
    ]

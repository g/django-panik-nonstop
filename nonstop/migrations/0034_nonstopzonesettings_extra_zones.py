# Generated by Django 1.11.29 on 2020-10-21 09:55

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('emissions', '0015_auto_20200404_1510'),
        ('nonstop', '0033_jingle_emission'),
    ]

    operations = [
        migrations.AddField(
            model_name='nonstopzonesettings',
            name='extra_zones',
            field=models.ManyToManyField(blank=True, related_name='extra_zones', to='emissions.Nonstop'),
        ),
    ]

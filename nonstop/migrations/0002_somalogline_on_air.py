from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('nonstop', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='somalogline',
            name='on_air',
            field=models.NullBooleanField(verbose_name=b'On Air'),
            preserve_default=True,
        ),
    ]

from django import forms
from django.utils.translation import gettext_lazy as _

from . import widgets
from .app_settings import app_settings
from .models import LANGUAGES, Jingle, Stream, Track


def get_nonstop_zones():
    from emissions.models import Nonstop

    return [(x.id, x.title) for x in Nonstop.objects.all()]


def get_optional_nonstop_zones():
    return [('', '')] + get_nonstop_zones()


def get_search_nonstop_zones():
    return (
        [('', ''), ('any', _('Any zone')), ('active', _('Active zones')), ('', '--------------')]
        + get_nonstop_zones()
        + [('', '--------------'), ('none', _('None'))]
    )


def get_jingle_choices():
    return [(x.id, x.label) for x in Jingle.objects.filter(emission__isnull=True, clock_time__isnull=True)]


def get_optional_jingle_choices():
    return [('', '')] + get_jingle_choices()


def get_stream_choices():
    return [(x.id, x.label) for x in Stream.objects.all()]


class ArtistForm(forms.Form):
    name = forms.CharField(label=_('Name'), required=True)


class MultipleFileField(forms.FileField):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('widget', widgets.MultipleClearableFileInput())
        super().__init__(*args, **kwargs)

    def clean(self, data, initial=None):
        single_file_clean = super().clean
        if isinstance(data, (list, tuple)):
            result = [single_file_clean(d, initial) for d in data]
        else:
            result = single_file_clean(data, initial)
        return result


class UploadTracksForm(forms.Form):
    tracks = MultipleFileField(
        label=_('Tracks'),
        widget=widgets.MultipleClearableFileInput(
            attrs={'multiple': True, 'accept': 'audio/ogg, audio/opus, audio/flac, audio/mpeg, audio/aac'}
        ),
    )
    nonstop_zone = forms.ChoiceField(label=_('Nonstop Zone'), choices=get_optional_nonstop_zones)


class TrackArtistTitleForm(forms.Form):
    artist = forms.CharField(label=_('Artist'), required=True)
    title = forms.CharField(label=_('Title'), required=True)


class TrackMetaForm(forms.ModelForm):
    class Meta:
        model = Track
        fields = [
            'language',
            'instru',
            'sabam',
            'cfwb',
            'gender_m',
            'gender_f',
            'gender_x',
            'gender_l',
            'nonstop_zones',
        ]
        widgets = {
            'nonstop_zones': widgets.MulticheckWidget,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['nonstop_zones'].queryset = self.fields['nonstop_zones'].queryset.order_by('start')
        if not app_settings.INCLUDE_SABAM_META:
            self.fields['sabam'].widget = forms.HiddenInput()
        if not app_settings.INCLUDE_CFWB_META:
            self.fields['cfwb'].widget = forms.HiddenInput()
        if not app_settings.INCLUDE_GENDER_META:
            self.fields['gender_m'].widget = forms.HiddenInput()
            self.fields['gender_f'].widget = forms.HiddenInput()
            self.fields['gender_x'].widget = forms.HiddenInput()
            self.fields['gender_l'].widget = forms.HiddenInput()


class TrackNonstopForm(forms.ModelForm):
    class Meta:
        model = Track
        fields = [
            'nonstop_zones',
        ]
        widgets = {
            'nonstop_zones': widgets.MulticheckWidget,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['nonstop_zones'].queryset = self.fields['nonstop_zones'].queryset.order_by('start')


class TrackSearchForm(forms.Form):
    q = forms.CharField(label=_('Text'), required=False)
    zone = forms.ChoiceField(label=_('Nonstop Zone'), choices=get_search_nonstop_zones, required=False)
    order_by = forms.ChoiceField(
        label=_('Order'),
        required=False,
        choices=[
            ('title', _('Alphabetically')),
            ('-added_to_nonstop_timestamp', _('Newest first')),
            ('added_to_nonstop_timestamp', _('Oldest first')),
        ],
    )


class CleanupForm(forms.Form):
    zone = forms.ChoiceField(label=_('Nonstop Zone'), choices=get_optional_nonstop_zones)
    no_metadata = forms.BooleanField(label=_('Limit to tracks without metadata'), required=False)


class ZoneSettingsForm(forms.Form):
    start = forms.TimeField(label=_('Start'), widget=widgets.TimeWidget)
    end = forms.TimeField(label=_('End'), widget=widgets.TimeWidget)
    intro_jingle = forms.ChoiceField(
        label=_('Intro Jingle'), choices=get_optional_jingle_choices, required=False
    )
    jingles = forms.MultipleChoiceField(
        label=_('Jingles'), widget=widgets.MulticheckWidget, required=False, choices=get_jingle_choices
    )

    weight_lang_en = forms.IntegerField(
        label=_('Weight adjustment for English tracks'),
        min_value=-10,
        max_value=10,
        widget=widgets.RangeWidget,
        required=False,
    )
    weight_lang_fr = forms.IntegerField(
        label=_('Weight adjustment for French tracks'),
        min_value=-10,
        max_value=10,
        widget=widgets.RangeWidget,
        required=False,
    )
    weight_lang_nl = forms.IntegerField(
        label=_('Weight adjustment for Dutch tracks'),
        min_value=-10,
        max_value=10,
        widget=widgets.RangeWidget,
        required=False,
    )
    weight_lang_other = forms.IntegerField(
        label=_('Weight adjustment for tracks in other languages'),
        min_value=-10,
        max_value=10,
        widget=widgets.RangeWidget,
        required=False,
    )
    weight_instru = forms.IntegerField(
        label=_('Weight adjustment for instrumental tracks'),
        min_value=-10,
        max_value=10,
        widget=widgets.RangeWidget,
        required=False,
    )
    weight_cfwb = forms.IntegerField(
        label=_('Weight adjustment for FWB tracks'),
        min_value=-10,
        max_value=10,
        widget=widgets.RangeWidget,
        required=False,
    )
    weight_newest = forms.IntegerField(
        label=_('Weight adjustement for newest tracks'),
        min_value=-10,
        max_value=10,
        widget=widgets.RangeWidget,
        required=False,
    )
    weight_new = forms.IntegerField(
        label=_('Weight adjustement for new tracks'),
        min_value=-10,
        max_value=10,
        widget=widgets.RangeWidget,
        required=False,
    )

    extra_zones = forms.MultipleChoiceField(
        label=_('Extra zones'), widget=widgets.MulticheckWidget, required=False, choices=get_nonstop_zones
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not app_settings.INCLUDE_CFWB_META:
            self.fields['weight_cfwb'].widget = forms.HiddenInput()


class RecurringPlaylistAddForm(forms.Form):
    jingle = forms.ChoiceField(label=_('Jingle'), choices=get_optional_jingle_choices, required=False)
    zones = forms.MultipleChoiceField(
        label=_('Zones'), widget=widgets.MulticheckWidget, required=False, choices=get_nonstop_zones
    )


class RecurringPlaylistEditForm(RecurringPlaylistAddForm):
    weight_lang_en = forms.IntegerField(
        label=_('Weight adjustment for English tracks'),
        min_value=-10,
        max_value=10,
        widget=widgets.RangeWidget,
        required=False,
    )
    weight_lang_fr = forms.IntegerField(
        label=_('Weight adjustment for French tracks'),
        min_value=-10,
        max_value=10,
        widget=widgets.RangeWidget,
        required=False,
    )
    weight_lang_nl = forms.IntegerField(
        label=_('Weight adjustment for Dutch tracks'),
        min_value=-10,
        max_value=10,
        widget=widgets.RangeWidget,
        required=False,
    )
    weight_lang_other = forms.IntegerField(
        label=_('Weight adjustment for tracks in other languages'),
        min_value=-10,
        max_value=10,
        widget=widgets.RangeWidget,
        required=False,
    )
    weight_instru = forms.IntegerField(
        label=_('Weight adjustment for instrumental tracks'),
        min_value=-10,
        max_value=10,
        widget=widgets.RangeWidget,
        required=False,
    )
    weight_cfwb = forms.IntegerField(
        label=_('Weight adjustment for FWB tracks'),
        min_value=-10,
        max_value=10,
        widget=widgets.RangeWidget,
        required=False,
    )
    weight_newest = forms.IntegerField(
        label=_('Weight adjustement for newest tracks'),
        min_value=-10,
        max_value=10,
        widget=widgets.RangeWidget,
        required=False,
    )
    weight_new = forms.IntegerField(
        label=_('Weight adjustement for new tracks'),
        min_value=-10,
        max_value=10,
        widget=widgets.RangeWidget,
        required=False,
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not app_settings.INCLUDE_CFWB_META:
            self.fields['weight_cfwb'].widget = forms.HiddenInput()


class RecurringDeleteForm(forms.Form):
    pass


class RecurringStreamForm(forms.Form):
    jingle = forms.ChoiceField(label=_('Jingle'), choices=get_optional_jingle_choices, required=False)
    stream = forms.ChoiceField(label=_('Stream'), choices=get_stream_choices, required=True)


class ArtistSearchForm(forms.Form):
    q = forms.CharField(label=_('Name'), required=False)
    zone = forms.ChoiceField(label=_('Nonstop Zone'), choices=get_search_nonstop_zones, required=False)


class TracksAddToZoneForm(forms.Form):
    zone = forms.ChoiceField(label=_('Nonstop Zone'), choices=get_nonstop_zones, required=True)


class TapeTrackForm(forms.Form):
    artist = forms.CharField(label=_('Artist'), required=True)
    title = forms.CharField(label=_('Title'), required=True)
    position = forms.CharField(label=_('Position'), help_text=_('Format: mm:ss'))

    instru = forms.BooleanField(label=_('Instru'), required=False)
    language = forms.ChoiceField(label=_('Language'), choices=[('', '')] + LANGUAGES, required=False)
    sabam = forms.BooleanField(label='SABAM', required=False)
    cfwb = forms.BooleanField(label='CFWB', required=False)

    gender_m = forms.IntegerField(label=_('(cis)Male members'), required=False, min_value=0)
    gender_f = forms.IntegerField(label=_('(cis)Female members'), required=False, min_value=0)
    gender_x = forms.IntegerField(
        label=_('Non-binary, trans or agender members'), required=False, min_value=0
    )
    gender_l = forms.ChoiceField(
        label=_('Lead'),
        choices=[
            ('', ''),
            ('m', _('Male')),
            ('f', ('Female')),
            ('x', _('Non-binary, trans or agender')),
        ],
        required=False,
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['position'].widget.attrs['placeholder'] = '--:--'
        if not app_settings.INCLUDE_SABAM_META:
            self.fields['sabam'].widget = forms.HiddenInput()
        if not app_settings.INCLUDE_CFWB_META:
            self.fields['cfwb'].widget = forms.HiddenInput()
        if not app_settings.INCLUDE_GENDER_META:
            self.fields['gender_m'].widget = forms.HiddenInput()
            self.fields['gender_f'].widget = forms.HiddenInput()
            self.fields['gender_x'].widget = forms.HiddenInput()
            self.fields['gender_l'].widget = forms.HiddenInput()


class TapeTrackEditForm(forms.Form):
    artist = forms.CharField(label=_('Artist'), required=True)
    title = forms.CharField(label=_('Title'), required=True)
    position = forms.CharField(label=_('Position'), help_text=_('Format: mm:ss'))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['position'].widget.attrs['placeholder'] = '--:--'
        # artist/title/metadata should be modified in their own objects
        self.fields['artist'].widget.attrs['readonly'] = 'readonly'
        self.fields['title'].widget.attrs['readonly'] = 'readonly'
